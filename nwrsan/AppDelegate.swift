//
//  AppDelegate.swift
//  nwrsan
//
//  Created by MACBOOK on 7/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FCAlertView
import SideMenu

let NormalFont = UIFont(name: "Cairo", size: 17)
let BoldFont = UIFont(name: "Cairo-Bold", size: 17)
let mainColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var reachability : Reachability!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuFadeStatusBar = false
        // Override point for customization after application launch.
        L102Localizer.DoTheMagic()
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = mainColor
        let ToolBarAppearace = UIToolbar.appearance()
        let BarAppearace = UINavigationBar.appearance()
        //        BarAppearace.barTintColor = .clear
        BarAppearace.setBackgroundImage(UIImage(), for: .default)
        BarAppearace.shadowImage = UIImage()
        BarAppearace.isTranslucent = true
        
        BarAppearace.titleTextAttributes = [
            NSAttributedStringKey.font: NormalFont!,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        ToolBarAppearace.backgroundColor = mainColor
        ToolBarAppearace.tintColor = UIColor.white
        let ButtonBarAppearace = UIBarButtonItem.appearance()
        ButtonBarAppearace.setTitleTextAttributes([
            NSAttributedStringKey.font: NormalFont as Any
            ], for: UIControlState.normal)
        ButtonBarAppearace.tintColor = UIColor.white
        
        //Keybored Setup
        IQKeyboardManager.shared.enable = true
        
        //For checking the Internet
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do {
            try self.reachability.startNotifier()
        } catch {
            
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //Get called when the wifi statue changed
    @objc func reachabilityChanged(notification:Notification) {
        let reachability = notification.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            makeDoneAlert(title: "من فضلك قم باعادة الاتصال بالانترنت مرة اخري", SubTitle: "", Image: #imageLiteral(resourceName: "img13"), color: UIColor.red)
            print("Network not reachable")
        }
    }
    
    // the alert to be Poped Up
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }



}

