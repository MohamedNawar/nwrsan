//
//  MainTabBar.swift
//  nwrsan
//
//  Created by MACBOOK on 9/11/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation
import UIKit


class MainTabBar: UITabBar {

    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 50
        return sizeThatFits
    }
    
    
    
}
