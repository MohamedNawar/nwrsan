import Foundation
import UIKit


    
    @IBDesignable
class ThreeColorsGradientView: UIButton {
        var firstColor: UIColor = #colorLiteral(red: 0.5960784314, green: 0.1137254902, blue: 0.7254901961, alpha: 1)
        var secondColor: UIColor = #colorLiteral(red: 0.2705882353, green: 0.5529411765, blue: 0.9843137255, alpha: 1)
        var thirdColor: UIColor = #colorLiteral(red: 0.2039215686, green: 0.831372549, blue: 0.6549019608, alpha: 1)
        
        @IBInspectable var vertical: Bool = true {
            didSet {
                updateGradientDirection()
            }
        }
        
        lazy var gradientLayer: CAGradientLayer = {
            let layer = CAGradientLayer()
            layer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
            layer.startPoint = CGPoint.zero
            return layer
        }()
        
        //MARK: -
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            applyGradient()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            applyGradient()
        }
        
        override func prepareForInterfaceBuilder() {
            super.prepareForInterfaceBuilder()
            applyGradient()
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            updateGradientFrame()
        }
        
        //MARK: -
        
        func applyGradient() {
            updateGradientDirection()
            layer.sublayers = [gradientLayer]
        }
        
        func updateGradientFrame() {
            gradientLayer.frame = bounds
        }
        
        func updateGradientDirection() {
            gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        }
    }


