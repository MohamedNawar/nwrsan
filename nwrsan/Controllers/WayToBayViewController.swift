//
//  WayToBayViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 8/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import TextFieldEffects
class WayToBayViewController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource  {
            var selectedAddress = Addresses()
            let picker = UIPickerView()
            let picker1 = UIPickerView()
            var productsCost = 0
            var totalPrice = 0
            var shippingPrice = 0 {
                didSet{
                   productsCost = myCart.data?.total?.amount ?? 0
                    totalPrice = productsCost + shippingPrice
                    totalPriceLbl.text = "\(totalPrice)"

                }
              }
            var words = [Addresses]()
            var selected = ""
            var myOrder = MyOrder()
            var selectedAddressId = Int()
            var myCart = MyCart(){
                didSet{
                    weightOfOrder.text = "\(myCart.data?.total_weight ?? 0)"
//                    , let currency = product.total?.currency
                    totalPriceLbl.text = "\(myCart.data?.total?.amount ?? 0) \(myCart.data?.total?.currency ?? "")"
                }
                }
            var userProfile = userData.Instance{
        didSet{
            words = (userProfile.data?.addresses)!
            self.picker.reloadAllComponents()
            self.navigationController?.navigationItem.title = "CheckOut"
        }
    }
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var addAddress: UIButton!
    @IBOutlet weak var weightOfOrder: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var shippingAddress: HoshiTextField!
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBAction func addAddressPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
                super.viewDidLoad()
        totalPriceLbl.text = "\(myCart.data?.total?.amount ?? 0) \(myCart.data?.total?.currency ?? "")"
        priceLbl.text = "\(0)"
        self.title = "إكمال الطلب"
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)

                picker.delegate = self
                picker.dataSource = self
                picker.backgroundColor = .white
                picker.showsSelectionIndicator = true
                picker1.delegate = self
                picker1.dataSource = self
                picker1.backgroundColor = .white
                picker1.showsSelectionIndicator = true
                userData.Instance.fetchUser()
                loadPersonalData()
                showAddressPicker()
        shippingAddress.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        loadMyCartData()
                
            }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadMyCartData()
        loadPersonalData()
    }
    
            func showAddressPicker(){
                let toolbar = UIToolbar();
                toolbar.sizeToFit()
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
                let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
                toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
                toolbar.barTintColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                shippingAddress.inputView = picker
                shippingAddress.inputAccessoryView = toolbar
            }
    
    @objc func cancel(){
        shippingAddress.text = ""
        self.view.endEditing(true)
    }
            @objc func donePicker(){
                if shippingAddress.text == "" {
                    shippingAddress.text = words[0].address ?? ""
                    selectedAddressId = words[0].id ?? 0
                    shippingPrice = userProfile.data?.addresses?[0].shipping_price?.amount ?? 0
                    let shippingCurrecy = userProfile.data?.addresses?[0].shipping_price?.currency ?? ""
                    priceLbl.text = "\(shippingPrice) \(shippingCurrecy)"
                }
                self.view.endEditing(true)
            }
    
            func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
                if pickerView == picker {
                    return words.count
                }
                return 0
            }
    
            func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
                if pickerView == picker {
                    return String(describing: words[row].address!)
                    
                }
                return ""
            }
    
            func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
                if pickerView == picker {
                    selected = String(describing: words[row].address ?? "")
//                    shippingPrice =
                    priceLbl.text = "\(userProfile.data?.addresses?[row].shipping_price?.amount ?? 0) \(userProfile.data?.addresses?[row].shipping_price?.currency ?? "")"
                    
                    
                    selectedAddressId = words[row].id ?? 0

                    shippingAddress.text = selected
                }
            }
    
            func numberOfComponents(in pickerView: UIPickerView) -> Int {
                return 1
            }
    
    func loadPersonalData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showProfile() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.userProfile = try JSONDecoder().decode(userData.self, from: response.data!)
                        print(self.userProfile)
                       
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم")
, andButtons: nil)
    }
    func checkOutPostData(){
        if self.words.count == 0  {
            HUD.flash(.label("Please add a Shipping Address"), delay: 1.0)
            return
        }
        if self.shippingAddress.text == ""  {
            HUD.flash(.label("Please Choose a Shipping Address "), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["address_id":selectedAddressId] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.Checkout(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    do {
                        print("successsss")
                        self.myOrder = try JSONDecoder().decode(MyOrder.self, from: response.data!)
//                        let lockString = NSLocalizedString("Your order is successfully being placed", comment: "لقد تم الطلب بنجاح")
                        
                        
                        let popUp = self.storyboard?.instantiateViewController(withIdentifier: "DonePopUp") as! DonePopUp
                        popUp.handleBack = { () in
                        self.navigationController?.popToRootViewController(animated: true)
                        }
//                        self.navigationController?.popToRootViewController(animated: true)
                        self.tabBarController?.present(popUp, animated: true, completion: nil)
                        
                        
//                        self.makeDoneAlert(title: lockString, SubTitle: lockString2, Image: #imageLiteral(resourceName: "img06"))
                        userData.Instance.identifier = ""
                        UserDefaults.standard.removeObject(forKey: "identifier")
                        userData.Instance.fetchUser()
                        print(userData.Instance.identifier);
                        print(self.myOrder)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
//                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    func loadMyCartData(){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.shoppingCartList() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        print(self.myCart)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

    @IBAction func checkOut(_ sender: Any) {
        checkOutPostData()
    }
    
}
