//
//  CartViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage

class CartViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    var cartItem = [MyCartDetailsItems]()
//    var alertController = UIAlertController()
    var myCart = MyCart(){
        didSet{
            CartsTableView.reloadData()
            cartItem = (myCart.data?.items)!
            totalPrice.text = "\(myCart.data?.total?.amount ?? 0) \(myCart.data?.total?.currency ?? "")"
            Cart.Instance.count = myCart.data?.items?.count ?? 0
        }
    }
    @IBOutlet weak var couponTxtField: UITextField!
    @IBOutlet weak var CartsTableView: UITableView!
    @IBOutlet weak var totalPrice: UILabel!
//    var subTotal = Int(){
//        didSet{
//            totalPrice.text = "\(subTotal)"
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
//        self.title = "عربة التسوق"
        CartsTableView.dataSource = self
        CartsTableView.delegate = self
        CartsTableView.separatorStyle = .none
        userData.Instance.fetchUser()
//        AlertControllerToCheckOut()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        print(userData.Instance.identifier)
        if userData.Instance.identifier == nil || userData.Instance.identifier == ""{
            loadMyCartData(type: true)
        }else{
            loadMyCartData(type: false)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == CartsTableView {
            if let count = myCart.data?.items?.count  {
                return count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CartsTableView.dequeueReusableCell(withIdentifier: "cartsCell", for: indexPath) as! CartTableViewCell
        if let productData = myCart.data?.items?[indexPath.row] {
        cell.updateViews(product: productData )
        }
        cell.quentityChanged = { () in
            self.setupTotal(index: indexPath.row,quentity: cell.quentity)
            self.updateCart(qty:cell.quentity, itemId: (self.myCart.data?.items?[indexPath.row].id)!)
        }
        cell.cartCallBack = {
            () -> () in
            self.mangeRemoveFromCart( itemId: (self.myCart.data?.items?[indexPath.row].id)!)
        }
        return cell
    }
    private func mangeRemoveFromCart(itemId:Int){
        removeCart(itemId: itemId)
    }

    private func setupTotal(index:Int,quentity:Int){
        myCart.data?.items?[index].item?.qty = quentity
//        self.subTotal = 0
//        for i in cartItem {
//            if let temp = i.item?.qty {
//                self.subTotal += Int(i.item?.price?.amount ?? 0) * temp
//            }
//        }
    }
    func loadMyCartData(type:Bool){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.shoppingCartList() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        
                        print(self.myCart)
                        if self.myCart.data?.items?.count == 0  {
                            HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.CartsTableView)
                        }
                        if type {
                            userData.Instance.identifier = self.myCart.data?.identifier
                            UserDefaults.standard.set(self.myCart.data?.identifier, forKey: "identifier")
                        }
                        
                        print(userData.Instance.identifier)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }

    @IBAction func Addcoupon(_ sender: Any) {
        PostAddcoupon()
    }
    func PostAddcoupon(){
        if couponTxtField.text == "" {
            
        HUD.flash(.label(NSLocalizedString("Enter you coupon", comment: "بالرجاء ادخال الكوبون")), delay: 1.0)
        return
        }
        let header = APIs.Instance.getHeader()
        let par = ["coupon": couponTxtField.text!] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addCoupon(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    HUD.flash(.success, delay: 1.0)
                    
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    func updateCart(qty:Int , itemId:Int){
        let header = APIs.Instance.getHeader()
        let par = ["qty": qty] as [String : Int]
        print(par)
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.UpdateItemInShoppingCart(id: itemId), method: .put, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
//                        self.
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func removeCart(itemId:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.DeleteItemInShoppingCart(id: itemId), method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        //                        self.
                        HUD.flash(.success, delay: 1.0)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
//    func AlertControllerToCheckOut() {
//        alertController = UIAlertController(title: "", message: "You have to Sign In to Check Out", preferredStyle: UIAlertControllerStyle.actionSheet)
//        let SignIn = UIAlertAction(title: "SignIn", style: UIAlertActionStyle.default) { (action) in
//            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//            self.navigationController?.pushViewController(navVC, animated: true)
//        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
//        alertController.addAction(SignIn)
//        alertController.addAction(cancelAction)
//
//    }
    @IBAction func goToCheckOut(_ sender: Any) {
        if userData.Instance.token == "" || userData.Instance.token == nil {
            let popUp = storyboard?.instantiateViewController(withIdentifier: "LoginPopUp") as! LoginPopUp
            popUp.modalPresentationStyle = .overCurrentContext
            popUp.handleBack = {(flag) in
                if flag {
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    
                    self.navigationController?.pushViewController(navVC, animated: true)
                }else{
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUPViewController") as! SignUPViewController
                    
                    self.navigationController?.pushViewController(navVC, animated: true)
                }
            }
            self.tabBarController?.present(popUp, animated: true, completion: nil)
        }else if (myCart.data?.items?.isEmpty) ?? true{
            let lockString = NSLocalizedString("Cart Empty", comment: "السلة فارغة")
            HUD.flash(.label(lockString), onView: self.view)
            
        }else{
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "WayToBayViewController") as! WayToBayViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (myCart.data?.items?[indexPath.row].item?.customAttributes?.count ?? 0) < 1 {
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        var att = [Attributes]()
        for i in myCart.data?.items?[indexPath.row].item?.customAttributes ?? []{
            let a = Attributes()
            let b = Values()
            
            a.id = i.value_id
            a.type = i.type
            a.name = i.attribute
            b.value = i.value
            a.values = [b]
            att.append(a)
        }
        vc.productData2.selectable_attributes = att
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
