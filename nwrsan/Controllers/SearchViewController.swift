//
//  SearchViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 10/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Font_Awesome_Swift
import TextFieldEffects
import PKHUD
import Alamofire
import FCAlertView
class SearchViewController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource  {
    
    
    @IBAction func changeValue(_ sender: Any) {
        let temp = has_offer
        has_offer = !temp
    }
    
    var brand_id : Int?
    var brand_name :String?
    var category_id : Int?
    var category_name :String?
    var subcategory_id : Int?
    var subcategory_name :String?
    
    
    
    
    
    
    @IBOutlet weak var productName: UITextField!
    
    @IBOutlet weak var productCode: HoshiTextField!
    @IBOutlet weak var merchant_Name: HoshiTextField!
    
    @IBOutlet weak var price_to: HoshiTextField!
    @IBOutlet weak var price_from: HoshiTextField!
    
    @IBOutlet weak var weight_from: HoshiTextField!
    @IBOutlet weak var weight_to: HoshiTextField!
    
    @IBOutlet weak var qty_from: HoshiTextField!
    
    @IBOutlet weak var qty_to: HoshiTextField!
    var categoriesData = Categories()
    var subCategoriesData = CategoriesSections()
    var brandData = Brands()
    var has_offer = false
    
    @IBOutlet weak var brandTxtField: HoshiTextField!
    @IBOutlet weak var subCategoryTxtField: HoshiTextField!
    @IBOutlet weak var categoryTxtField: HoshiTextField!
    let CatergoriesPicker = UIPickerView()
    let SubCategoriesPicker = UIPickerView()
    let brandsPicker = UIPickerView()
    
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBOutlet weak var QtyLbl: UILabel!
    
    @IBOutlet weak var weightLbl: UILabel!
    
    
    @IBOutlet weak var hasOfferLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Search", comment: "البحث")
        hasOfferLbl.text = NSLocalizedString("Has Offer", comment: "يحتوي علي عرض")
        productName.placeholder = NSLocalizedString("Name", comment: "الاسم")
        
        productCode.placeholder = NSLocalizedString("Code", comment: "الكود")
        merchant_Name.placeholder = NSLocalizedString("Merchant Name", comment: "اسم البائع")
        
        price_to.placeholder = NSLocalizedString("To", comment: "إلي")
        price_from.placeholder = NSLocalizedString("From", comment: "من")
        
        weight_from.placeholder = NSLocalizedString("From", comment: "من")
        weight_to.placeholder = NSLocalizedString("To", comment: "إلي")
        
        qty_from.placeholder = NSLocalizedString("From", comment: "من")
        
        qty_to.placeholder = NSLocalizedString("To", comment: "إلي")
        
        
        priceLbl.text = NSLocalizedString("Price", comment: "السعر")
        
        weightLbl.text = NSLocalizedString("Weight", comment: "الوزن")
        
        QtyLbl.text = NSLocalizedString("Quantity", comment: "الكمية")
        
        categoryTxtField.placeholder = NSLocalizedString("Category", comment: "الأقسام")
        
        subCategoryTxtField.placeholder = NSLocalizedString("Sub Category", comment: "الأقسام الفرعية")
        
        brandTxtField.placeholder = NSLocalizedString("Brand", comment: "الماركات")
        
        
        categoryTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        subCategoryTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        brandTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        pickerInitialize()
        showCategoryPicker()
        showSubCategoryPicker()
        showBrandPicker()
        loadCategoriesData()
        loadBrands()

    }
    func pickerInitialize(){
        CatergoriesPicker.delegate = self
        CatergoriesPicker.dataSource = self
        CatergoriesPicker.backgroundColor = .white
        CatergoriesPicker.showsSelectionIndicator = true
        SubCategoriesPicker.delegate = self
        SubCategoriesPicker.dataSource = self
        SubCategoriesPicker.backgroundColor = .white
        SubCategoriesPicker.showsSelectionIndicator = true
        brandsPicker.delegate = self
        brandsPicker.dataSource = self
        brandsPicker.backgroundColor = .white
        brandsPicker.showsSelectionIndicator = true
    }
    func showCategoryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        categoryTxtField.inputView = CatergoriesPicker
        categoryTxtField.inputAccessoryView = toolbar
    }
    
    @objc func cancel(){
        categoryTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if categoryTxtField.text == "" {
            categoryTxtField.text = category_name ?? ""
        }
        self.view.endEditing(true)
    }
    func showSubCategoryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        subCategoryTxtField.inputView = SubCategoriesPicker
        subCategoryTxtField.inputAccessoryView = toolbar
    }
    
    @objc func cancel1(){
        subCategoryTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker1(){
        if subCategoryTxtField.text == "" {
            subCategoryTxtField.text = subcategory_name ?? ""
        }
        self.view.endEditing(true)
    }
    func showBrandPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker2));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain , target: self, action: #selector(cancel2))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        brandTxtField.inputView = brandsPicker
        brandTxtField.inputAccessoryView = toolbar
    }
    
    @objc func cancel2(){
        brandTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker2(){
        if brandTxtField.text == "" {
            brandTxtField.text = brand_name ?? ""
        }
        self.view.endEditing(true)
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var StrArr = [String]()
        var idArr = [Int]()
        if pickerView == CatergoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in categoriesData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return StrArr.count
        }
        if pickerView == SubCategoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in subCategoriesData.data?.subcategories ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return StrArr.count
        }
        if pickerView == brandsPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in brandData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return StrArr.count
        }
        
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var StrArr = [String]()
        var idArr = [Int]()
        if pickerView == CatergoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in categoriesData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return "\(StrArr[row])"
            
        }
        if pickerView == SubCategoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in subCategoriesData.data?.subcategories ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return "\(StrArr[row])"
        }
        if pickerView == brandsPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in brandData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            return "\(StrArr[row])"
            
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var StrArr = [String]()
        var idArr = [Int]()
        
        if pickerView == CatergoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in categoriesData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            self.category_name = StrArr[row]
            self.category_id = idArr[row]
            self.subCategoriesData.data = self.categoriesData.data?[row] ?? CategoriesData()
            categoryTxtField.text = self.category_name
            self.dismiss(animated: true, completion: nil)
        }
        if pickerView == SubCategoriesPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in subCategoriesData.data?.subcategories ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            self.subcategory_name = StrArr[row]
            self.subcategory_id = idArr[row]
            subCategoryTxtField.text = self.subcategory_name
            self.dismiss(animated: true, completion: nil)
        }
        if pickerView == brandsPicker {
            StrArr.removeAll()
            idArr.removeAll()
            for i in brandData.data ?? []{
                StrArr.append(i.name ?? "")
                idArr.append(i.id ?? -1)
            }
            self.brand_name = StrArr[row]
            self.brand_id = idArr[row]
            brandTxtField.text = self.brand_name
        }
        }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    //SubCategories
    func loadSubCategoriesData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showCategory(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.subCategoriesData = try JSONDecoder().decode(CategoriesSections.self, from: response.data!)
                        
                        print(self.categoriesData)
                        
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    //brands
    func loadBrands(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.listingBrands() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.brandData = try JSONDecoder().decode(Brands.self, from: response.data!)
                        print(self.brandData)
                        
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    //Categories
    func loadCategoriesData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.listingCategories() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.categoriesData = try JSONDecoder().decode(Categories.self, from: response.data!)
                        print(self.categoriesData)
                        
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    
    @IBAction func gotoProducts(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductsVC") as! ProductsVC
        vc.name = productName.text
        vc.price_from = Int(self.qty_from.text!)
        vc.price_to = Int(self.qty_to.text!)
        vc.brand_id = self.brand_id
        
        vc.category_id = self.category_id
        
        vc.subcategory_id = self.subcategory_id
        
        vc.weight_from = Int(self.price_from.text!)
        vc.weight_to = Int(self.price_to.text!)
        vc.code = self.productCode.text
        vc.qty_to = Int(self.weight_to.text!)
        vc.qty_from = Int(self.weight_from.text!)
        vc.has_offer = self.has_offer
        vc.merchant_name = merchant_Name.text
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
