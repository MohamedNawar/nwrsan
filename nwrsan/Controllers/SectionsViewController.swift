//
//  SectionsViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/31/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

enum FlowLayoutType: Int {
    case tags
    case pinterest
    case px500
    case instagram
    case flipboard
    case facebook
    case flickr
}


import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import collection_view_layouts

class SectionsViewController: UIViewController, UICollectionViewDataSource , UICollectionViewDelegate , ContentDynamicLayoutDelegate   {
    func cellSize(indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 200)
    }
    var colorArr = [#colorLiteral(red: 0.9450980392, green: 0.9411764706, blue: 0.9607843137, alpha: 1),#colorLiteral(red: 0.9058823529, green: 0.9490196078, blue: 0.9058823529, alpha: 1),#colorLiteral(red: 0.8588235294, green: 0.9215686275, blue: 0.9843137255, alpha: 1)]
    var realColorArr = [UIColor]()
    var id = Int()
    private var contentFlowLayout: ContentDynamicLayout?
    var categoriesData = Categories(){
        didSet{
            setupColor()
            SectionsCollectionView.reloadData()
        }
    }
    
    private func setupColor(){
        for i in 0..<(categoriesData.data?.count ?? 0){
            if i % 3 == 0{
                realColorArr.append(contentsOf: colorArr)
            }
        }
    }
        @IBOutlet weak var SectionsCollectionView: UICollectionView!
        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            
            
            SectionsCollectionView.delegate = self
            SectionsCollectionView.dataSource = self
            setupCollectionView()
            loadCategoriesData()
            userData.Instance.fetchUser()
        }
    private func setupCollectionView() {
        SectionsCollectionView.dataSource = self
        showLayout()
    }
    private func showLayout() {
            contentFlowLayout = FlickrStyleFlowLayout()
        contentFlowLayout?.delegate = self
        contentFlowLayout?.contentPadding = ItemsPadding(horizontal: -20, vertical: -5)
        contentFlowLayout?.cellsPadding = ItemsPadding(horizontal: -15, vertical: -15)
        SectionsCollectionView.collectionViewLayout = contentFlowLayout!
        SectionsCollectionView.setContentOffset(CGPoint.zero, animated: false)
    }
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if collectionView == SectionsCollectionView {
                if let count = categoriesData.data?.count  {
                    return count
                }
            }
            return 0

        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SectionsCell", for: indexPath)
            cell.clipsToBounds = true
            if let  imageIndex = categoriesData.data?[indexPath.row].image {
                print(imageIndex)
                let image = cell.viewWithTag(1) as! UIImageView
                image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }
            cell.backgroundColor = realColorArr[indexPath.row]
            if let nameValue = categoriesData.data?[indexPath.row].name {
                let name = cell.viewWithTag(2) as! UILabel
                name.text = "\(nameValue)"
            }
            return cell
        }
    
    func loadCategoriesData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.listingCategories() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.categoriesData = try JSONDecoder().decode(Categories.self, from: response.data!)
                        if  self.categoriesData.data?.count == 0  {
                            HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.SectionsCollectionView)
                        }
                        print(self.categoriesData)
                        self.SectionsCollectionView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            id = (categoriesData.data?[indexPath.row].id)!
            performSegue(withIdentifier: "sectionsBranches", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sectionsBranches" {
            let destination = segue.destination as? SubCategoryVC
            destination?.categoryId = id
        }
    }
    
    }
extension SectionsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    }
            

