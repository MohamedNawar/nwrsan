

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import Auk
import moa

struct cellData {
    var open = Bool()
    var title = String()
}


class ProductVC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
    
    
    
    
    var product = ProductsData()
    var attsId = [Int]()
    var valuesId = [Int]()
    
    var quentity = 1{
        didSet{
            numberOfProductLbl.text = "\(quentity)"
        }
    }
    var productData = ProductsSection(){
        didSet{
            createAttArr()
            setScrollView(product: productData)
            DetailsTable.reloadData()
            self.title = productData.data?.name ?? ""
        }
    }
    var myCart = MyCart()
    var tableViewData = [cellData]()
    
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var DetailsTable: UITableView!
     @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var numberOfProductLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBAction func addRate(_ sender: UIButton) {
        if userData.Instance.token == "" || userData.Instance.token == nil {
            let popUp = storyboard?.instantiateViewController(withIdentifier: "LoginPopUp") as! LoginPopUp
            popUp.modalPresentationStyle = .overCurrentContext
            popUp.handleBack = {(flag) in
                if flag {
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    
                    self.navigationController?.pushViewController(navVC, animated: true)
                }else{
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUPViewController") as! SignUPViewController
                    
                self.navigationController?.pushViewController(navVC, animated: true)
                }
            }
            self.tabBarController?.present(popUp, animated: true, completion: nil)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReviewsVC") as! ReviewsVC
        vc.productId = product.id ?? -1
        vc.handleBack = {() in
            self.loadData(id: self.productData.data?.id ?? -1)
        }
        self.navigationController?.show(vc, sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = productData.data?.name ?? ""
        self.productData.data = product
       scrollView.auk.settings.contentMode = .scaleToFill
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        DetailsTable.delegate = self
        DetailsTable.dataSource = self
        DetailsTable.allowsSelection = false
        userData.Instance.fetchUser()
        loadData(id: productData.data?.id ?? -1)
        if productData.data?.is_favorite?.BBool == true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
        }else{
            favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
        }
        
//        self.hero.isEnabled = true
//        self.view.hero.id = "ironMan"
        
        
    }


    private func createAttArr(){

        for _ in (productData.data?.selectable_attributes ?? []){
            attsId.append(-1)
            valuesId.append(-1)
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = productData.data?.name ?? ""
        if productData.data?.is_favorite?.BBool == true {
         favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
        }else{
           favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
        }
    }
    
    func setScrollView(product:ProductsSection){
        if let images = product.data?.images {
            for image in images {
                scrollView.auk.show(url:(image.large) ?? "")
                
                
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
        return 1
        }
        if section == 1 {
            return productData.data?.selectable_attributes?.count ?? 0
        }
        if section == 2 {
            return productData.data?.additional_attributes?.count ?? 0
        }
        if section == 3 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.section == 0 {
        if indexPath.row == 0 {
         let cell1 =  DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath)
            if let  imageIndex = productData.data?.brand?.logo {
                print(imageIndex)
                let image = cell1.viewWithTag(1) as! UIImageView
                image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }
            if let nameValue = productData.data?.name{
                let name = cell1.viewWithTag(2) as! UILabel
                name.text = "\(nameValue)"
                self.navigationController?.navigationItem.title = "\(nameValue)"
            }
            if let priceValue = productData.data?.new_price?.amount , let currency = productData.data?.new_price?.currency {
                let name = cell1.viewWithTag(3) as! UILabel
                name.text = "\(priceValue) \(currency)"
            }
            if let sale = productData.data?.discount_percentage{
                
                let oldPriceLbl = cell1.viewWithTag(-22) as! UILabel
                
                oldPriceLbl.text = " \(String(sale)) % "
                
            }
            if let descriptionValue = productData.data?.description{
                let name = cell1.viewWithTag(10) as! UILabel
                name.text = "\(descriptionValue)"
            }
            if let codeValue = productData.data?.code{
                let name = cell1.viewWithTag(-4) as! UILabel
                name.text = "\(codeValue)"
            }

            return cell1
        }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                if productData.data?.rating?.rating_count == 0 {
                    let tempCell = UITableViewCell()
                    tempCell.textLabel?.text = NSLocalizedString("No rating", comment: "لا يوجد تقييم")
                    tempCell.backgroundColor = .clear
                    tempCell.textLabel?.font = NormalFont
                    tempCell.textLabel?.textAlignment = .center
                    tempCell.textLabel?.tag = -1
                    
                    return tempCell
                }else{
                    let cell2 =
                        DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell3", for: indexPath) as! RattingCell
                    cell2.ratting = productData.data?.rating ?? Rating()
                    return cell2
                }
        }
    }
    else if indexPath.section == 1 {
       if let selectableAttributes = productData.data?.selectable_attributes {
            if selectableAttributes[indexPath.row].type == "color" {
                let cell2 =
              DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell2", for: indexPath) as! ColorTableViewCell
                if let title = selectableAttributes[indexPath.row].name{
                    let name = cell2.viewWithTag(1) as! UILabel
                    name.text = "\(title)"
                }
                cell2.productDataContent = selectableAttributes[indexPath.row]
                cell2.colorCallBack = {
                    (index: Int,flag:Int) -> Void in
                    
                    self.attsId[indexPath.row] = selectableAttributes[indexPath.row].id ?? -1
                    self.valuesId[indexPath.row] = selectableAttributes[indexPath.row].values?[index].id ?? -1

                }
                return cell2
            }
            if selectableAttributes[indexPath.row].type == "string" {
            let cell5 =
                DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                if let title = selectableAttributes[indexPath.row].name{
                    let name = cell5.viewWithTag(2) as! UILabel
                    name.text = "\(title)"
                }
            cell5.productDataContent = selectableAttributes[indexPath.row]
                cell5.lblCallBack = {
                    (index: Int,flag:Int) -> Void in
                    if flag == 1 {
                        self.attsId[indexPath.row] = selectableAttributes[indexPath.row].id ?? -1
                        self.valuesId[indexPath.row] = selectableAttributes[indexPath.row].values?[index].id ?? -1
                    }
                }

            return cell5
        }
           if selectableAttributes[indexPath.row].type == "number" {
                let cell22 =
                    DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                if let title = selectableAttributes[indexPath.row].name{
                    let name = cell22.viewWithTag(2) as! UILabel
                    name.text = "\(title)"
                }
                cell22.productDataContent = selectableAttributes[indexPath.row]
                cell22.lblCallBack = {
                (index: Int,flag:Int) -> Void in
                if flag == 2 {
                    self.attsId[indexPath.row] = selectableAttributes[indexPath.row].id ?? -1
                    self.valuesId[indexPath.row] = selectableAttributes[indexPath.row].values?[index].id ?? -1
                }
            }
                return cell22
            }
            if selectableAttributes[indexPath.row].type == "date" {
                let cell22 =
                    DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                if let title = selectableAttributes[indexPath.row].name{
                    let name = cell22.viewWithTag(2) as! UILabel
                    name.text = "\(title)"
                }
                cell22.productDataContent = selectableAttributes[indexPath.row]
                cell22.lblCallBack = {
                    (index: Int,flag:Int) -> Void in
                    if flag == 3 {
                        self.attsId[indexPath.row] = selectableAttributes[indexPath.row].id ?? -1
                        self.valuesId[indexPath.row] = selectableAttributes[indexPath.row].values?[index].id ?? -1
                    }
                }
                return cell22
        }
    }

    }else if indexPath.section == 2 {
            if (productData.data?.additional_attributes?.count ?? 0) > 0 {
                let additionalAttributes = productData.data?.additional_attributes
                let cell222 =
                    DetailsTable.dequeueReusableCell(withIdentifier: "DetailsCell222", for: indexPath) as! ThirdTableViewCell
                if let title = additionalAttributes?[indexPath.row].name{
                    let name = cell222.viewWithTag(21) as! UILabel
                    name.text = "\(title)"
                }
                cell222.productDataContent = (additionalAttributes?[indexPath.row])!
                return cell222
            }

            }
       return cell
    }
    
    func loadData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showProduct(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.productData = try JSONDecoder().decode(ProductsSection.self, from: response.data!)
                        print(self.productData)
                        
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func addOne(_ sender: Any) {
        quentity += 1
    }

    @IBAction func removeOne(_ sender: Any) {
        quentity -= 1
        if quentity < 1 {
            quentity = 1
        }
    }
    @IBAction func addtoCart(_ sender: Any) {
        addToCart(productID: productData.data?.id ?? 0 , qty: quentity)
    }
    func addToCart(productID:Int , qty:Int){
        if (productData.data?.additional_attributes?.count ?? 0) > 0 {
            for i in valuesId {
                if i == -1{

                    let str = NSLocalizedString("Select Your Product Attributes", comment: "بالرجاء اكمال الاختيارات")
                    HUD.flash(.label(str), delay: 1.0)
                    return
                }
            }
        }
        var par = ["product_id": productID , "qty": qty ] as [String : Any]
        for i in 0..<valuesId.count{
           par["customAttributes[\(attsId[i])]"] = valuesId[i]
        }
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addProductToShoppingCart(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }else{
                    
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        print("successsss")
                        print("successsss")
                        self.DetailsTable.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    Cart.Instance.count += 1
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    @IBAction func addToFavourite(_ sender: Any) {
        addToFavorite(productID: productData.data?.id ?? 0)
    }
    func addToFavorite(productID:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddToFavorites(id: productID), method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        let productData = try JSONDecoder().decode(ProductsSection.self, from: response.data!)
                        if productData.data?.is_favorite?.BBool == true {
                            self.favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
                        }else{
                            self.favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
                        }
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    @IBAction func sareProductUrl(_ sender: Any) {
        let url = APIs.Instance.showProduct(id: productData.data?.id ?? 0)
        print(url)
        let activityController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
//    @IBAction func addProductToCart(_ sender: Any) {
//        addToCart(productID: productData.data?.id ?? 0 , qty: quentity)
//    }
    
    @IBAction func copyCode(_ sender: Any) {
    UIPasteboard.general.string = productData.data?.code ?? ""
         HUD.flash(.label("The code has been copied"), delay: 1.0)
    }
    
    //    private func postRate(){
    //        let header = APIs.sharedInstance.getHeader()
    //        let par = ["value": rate.value,
    //                   "comment": comment.text!] as [String : Any]
    //        print(par)
    //
    //        HUD.show(.progress, onView: self.view)
    //        Alamofire.request(APIs.sharedInstance.postRate(id: shop.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
    //            switch(response.result) {
    //            case .success(let value):
    //                HUD.hide()
    //                print(value)
    //                let temp = response.response?.statusCode ?? 400
    //                if temp >= 300 {
    //
    //                    do {
    //                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
    //                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
    //                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
    //                        //                            user.saveUser(user: user)
    //                    }catch{
    //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
    //                    }
    //                }else{
    //                    self.makeDoneAlert(title: "شكرا", SubTitle: "", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
    //                    self.FinishPressed?()
    //                    self.dismiss(animated: true, completion: nil)
    //                }
    //            case .failure(_):
    //                HUD.hide()
    //                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
    //                break
    //            }
    //        }
    //    }

}

