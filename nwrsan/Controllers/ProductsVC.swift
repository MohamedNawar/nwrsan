//
//  BrandProductViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 9/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//
import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class ProductsVC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    var name : String?
    var price_from : Int?
    var price_to : Int?
    var brand_id : Int?
    var category_id : Int?
    var subcategory_id : Int?
    var weight_from : Int?
    var weight_to : Int?
    var code : String?
    var qty_to : Int?
    var qty_from : Int?
    var has_offer : Bool?
    var merchant_name : String?
    
    
    
    
    
    var productSection = ProductsSection(){
        didSet{
            BrandCategoryTestCollectionView.reloadData()
        }
    }
    
//    var myCart = MyCart()
    var products = Products(){
        didSet{
            BrandCategoryTestCollectionView.reloadData()
        }
    }
    @IBOutlet weak var BrandCategoryTestCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.title = ""
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        

        BrandCategoryTestCollectionView.delegate = self
        BrandCategoryTestCollectionView.dataSource = self
        userData.Instance.fetchUser()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = products.data?.count {
            return count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.BrandCategoryTestCollectionView.frame.width / 2) - 10 , height: 220 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandProductCell", for: indexPath) as? BrandProductCell
        cell?.updateViews(product: (products.data?[indexPath.row])!)
        if products.data?[indexPath.row].is_favorite?.BBool == true {
            cell?.favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
        }else{
            cell?.favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
        }
        cell?.favoriteCallBack = {
            () -> () in
            self.addToFavorite(productID: self.products.data?[indexPath.row].id ?? -1, index: indexPath.row)
        }
        cell?.cartCallBack = {
            () -> () in
            self.mangeAddToCart(index: indexPath.row)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
        vc.product = products.data?[indexPath.row] ?? ProductsData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func mangeAddToCart(index:Int){

        if let prod = products.data?[index] {
            addToCart(productTemp: prod, qty: 1)
        }
        
    }
    
    func getData(){
//        let par = ["subcategory_id  ": showProductWithBrand]
        
        var par = "?"
        
        if let temp = name{
            par = par + "name=\(temp)&"
        }
        if let temp = price_from{
            par = par + "price_from=\(temp)&"
        }
        if let temp = price_to{
            par = par + "price_to=\(temp)&"
        }
        if let temp = brand_id{
            par = par + "brand_id=\(temp)&"
        }
        if let temp = category_id{
            par = par + "category_id=\(temp)&"
        }
        if let temp = subcategory_id{
            par = par + "subcategory_id=\(temp)&"
        }
        if let temp = weight_from{
            par = par + "weight_from=\(temp)&"
        }
        if let temp = weight_to{
            par = par + "weight_to=\(temp)&"
        }
        if let temp = code{
            par = par + "code=\(temp)&"
        }
        if let temp = qty_to{
            par = par + "qty_to=\(temp)&"
        }
        if let temp = qty_from{
            par = par + "qty_from=\(temp)&"
        }
        if let temp = has_offer{
            par = par + "has_offer=\(temp)&"
        }
        if let temp = merchant_name{
            par = par + "merchant_name=\(temp)&"
        }
        
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        
        Alamofire.request(APIs.Instance.listingProducts() + par, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.products = try JSONDecoder().decode(Products.self, from: response.data!)
                        print(self.products)
                        
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    func addToFavorite(productID:Int,index:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddToFavorites(id: productID), method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                        
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode(ProductsSection.self, from: response.data!)
                        if let t = temp.data {
                           self.products.data?[index] = t
                            self.BrandCategoryTestCollectionView.reloadData()
                        }
                        

                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                break
            }
        }
        
    }
//    func removeFromFavorite(productID:Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.Instance.RemoveFromFavorites(id: productID), method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            print(response)
//            switch(response.result) {
//            case .success(let value):
//                HUD.hide()
//                let temp = response.response?.statusCode ?? 400
//                print(temp)
//                if temp >= 300 {
//                    print("errorrrr")
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
//                        print(err.errors?.password)
//                    }catch{
//                        print("errorrrrelse")
//
//                    }
//                }else{
//
//                    do {
//                        print("successsss")
//                    }catch{
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                    HUD.flash(.success, delay: 1.0)
//
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
//                break
//            }
//        }
//
//    }
    // see cart
    func addToCart(productTemp:ProductsData , qty:Int){
        if productTemp.selectable_attributes?.count != 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
            vc.product = productTemp
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        let par = ["product_id": productTemp.id ?? -1 , "qty": qty  ] as [String : Any]
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addProductToShoppingCart(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                       
                        print(err.parseError())
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    
}

