//
//  AddAddressViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import TextFieldEffects
import Font_Awesome_Swift
import PKHUD
import Alamofire
import FCAlertView

class AddAddressViewController: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource  {
        var selectedCountry = CountryData()
        var currentAddress = Addresses()
        let picker = UIPickerView()
        let picker1 = UIPickerView()
        var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
        var selected = ""
        var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
        var selected1 = ""
        var cityId :Int = 0
    var countryIndex :Int = 0
        @IBOutlet weak var NameTxtField: HoshiTextField!
        @IBOutlet weak var PhoneNumberTxtField: HoshiTextField!
        @IBOutlet weak var AddressInDetailTxtField: HoshiTextField!
        @IBOutlet weak var isDefult: UISwitch!
        @IBOutlet weak var chooseCityTxtField: HoshiTextField!
        @IBOutlet weak var chooseCountryTxtField: HoshiTextField!
        override func viewDidLoad() {
            self.navigationController?.navigationItem.title = "Edit Address"
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            let textAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
            super.viewDidLoad()
            picker.delegate = self
            picker.dataSource = self
            picker.backgroundColor = .white
            picker.showsSelectionIndicator = true
            picker1.delegate = self
            picker1.dataSource = self
            picker1.backgroundColor = .white
            picker1.showsSelectionIndicator = true
            chooseCountryTxtField.text = currentAddress.city?.country?.name
            chooseCityTxtField.text = currentAddress.city?.name
            AddressInDetailTxtField.text = currentAddress.fullname
            NameTxtField.text = currentAddress.name
            PhoneNumberTxtField.text = currentAddress.phone
            cityId = (currentAddress.city?.id) ?? 0
            print(cityId)
            
            loadCountryData()
            showCityPicker()
            showCountryPicker()
            userData.Instance.fetchUser()
        }
        func loadCountryData() {
            HUD.show(.progress, onView: self.view)
            CountryMain.Instance.getCountriesServer(enterDoStuff: { () in
                self.words = CountryMain.Instance.getCountryNameArr()
                print(self.words)
                self.picker.reloadAllComponents()
                HUD.hide(animated: true)
            })
        }
        func showCountryPicker(){
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker));
            
            let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain , target: self, action: #selector(cancel))
            
            toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
            toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            chooseCountryTxtField.inputView = picker
            chooseCountryTxtField.inputAccessoryView = toolbar
            
        }
        func showCityPicker(){
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done1Picker));
            let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain , target: self, action: #selector(cancel1))
            
            toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
            toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            chooseCityTxtField.inputView = picker1
            chooseCityTxtField.inputAccessoryView = toolbar
            
        }
        @objc func donePicker(){
            if chooseCountryTxtField.text == "" {
                if words.count > 0 {
                    chooseCountryTxtField.text = words[0]
                    selectedCountry = CountryMain.Instance.countryData.data[0]
                    countryIndex = 0
                    words1 = selectedCountry.getCitiesName()
                    self.picker1.reloadAllComponents()
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            self.view.endEditing(true)
            chooseCityTxtField.isEnabled = true
            
        }
        @objc func cancel(){
            chooseCountryTxtField.text = ""
            self.view.endEditing(true)
        }
        @objc func cancel1(){
            chooseCityTxtField.text = ""
            self.view.endEditing(true)
        }
        
        @objc func done1Picker(){
            if chooseCityTxtField.text == "" {
                if words1.count > 0 {
                    chooseCityTxtField.text = words1[0]
                    cityId =  CountryMain.Instance.countryData.data[0].id ?? 0
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            self.view.endEditing(true)
            
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView == picker {
                return words.count
            }
            if pickerView == picker1 {
                return words1.count
            }
            return 0
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if pickerView == picker {
                return "\(words[row])"
                
            }
            if pickerView == picker1 {
                return "\(words1[row])"
            }
            return ""
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView == picker {
                if words.count > 0 {
                    selectedCountry = CountryMain.Instance.countryData.data[row]
                    words1 = selectedCountry.getCitiesName()
                    chooseCountryTxtField.text =  words[row]
                    countryIndex = row
                    self.picker1.reloadAllComponents()
                    self.picker.reloadAllComponents()
                    
                }  else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
            if pickerView == picker1 {
                if words1.count > 0 {
                    selected1 = words1[row]
                    cityId = CountryMain.Instance.countryData.data[countryIndex].cities?[row].id ?? 0
                    chooseCityTxtField.text = selected1
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        func PostAddress(){
            if self.chooseCountryTxtField.text == "" || self.AddressInDetailTxtField.text == "" || self.chooseCityTxtField.text == "" || self.PhoneNumberTxtField.text == "" || self.NameTxtField.text == "" {
                return
            }
            let header = APIs.Instance.getHeader()
            let par = ["name": NameTxtField.text!, "city_id": cityId ,"phone": String(describing: PhoneNumberTxtField.text!) , "address": AddressInDetailTxtField.text! , "is_default":isDefult.isOn] as [String : Any]
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.addAddress(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                
                print(response)
                
                switch(response.result) {
                case .success(let value):
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    print(temp)
                    if temp >= 300 {
                        print("errorrrr")
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                            print(err.errors?.password)
                        }catch{
                            print("errorrrrelse")
                            
                        }
                    }else{
                        
                        
                        HUD.flash(.success, delay: 1.0)
                        self.navigationController?.popViewController(animated: true)
                    }
                case .failure(_):
                    HUD.hide()
                    HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                    break
                }
            }
            
        }
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
        }
        
        @IBAction func addAddress(_ sender: Any) {
            PostAddress()
        }
}
