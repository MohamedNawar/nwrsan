//
//  ContactUsViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import TextFieldEffects
//import ReCaptcha
class ContactUsViewController: UIViewController {
//    let recaptcha = try? ReCaptcha()
    
    @IBOutlet weak var NameHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nametextHeight: NSLayoutConstraint!
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var nameTxtField: UITextField!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var subjectLbl: UILabel!
    
    @IBOutlet weak var contentLbl: UILabel!
    
    //    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var titleOfMessageTxtField: UITextField!
    @IBOutlet weak var messageContentTxtView: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLbl.text = NSLocalizedString("Name", comment: "الاسم")
        subjectLbl.text = NSLocalizedString("Subject", comment: "العنوان")
       
       contentLbl.text = NSLocalizedString("Content", comment: "الرسالة")
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        userData.Instance.fetchUser()
        if userData.Instance.token != nil && userData.Instance.token != "" {
            nametextHeight.constant = 0
            NameHeight.constant = 0
        }
    }
   
    @IBAction func sendMessage(_ sender: Any) {
        sendMessage()
    }
    func sendMessage(){
        if  self.titleOfMessageTxtField.text == "" || self.messageContentTxtView.text == "" {
            HUD.flash(.label(NSLocalizedString("Please Enter you data", comment: "من فضلك قم بملئ البيانات")), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        var par = ["title": titleOfMessageTxtField.text!, "content": messageContentTxtView.text!] as [String : Any]
        
        if userData.Instance.token != nil && userData.Instance.token != "" {
            par["name"] = nameTxtField.text
        }
        
        
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.contactMessages() , method:.post , parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                          print("successsss")
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
