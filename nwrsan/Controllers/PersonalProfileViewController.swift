//
//  PersonalProfileViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage

class PersonalProfileViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UIImagePickerControllerDelegate , UINavigationControllerDelegate  {
    var flag = false
    var photoChaged = false
    var selectedAddress = Addresses()
    let imagePicker = UIImagePickerController()
    var alertController = UIAlertController()
    @IBOutlet weak var AddressesTableView: UITableView!
    @IBOutlet weak var emailTxtField: DesignableTextField!
    @IBOutlet weak var phoneTxtField: DesignableTextField!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var EditPersonalProfile: UIButton!
    @IBOutlet weak var nameTxtField: UILabel!
    
    @IBAction func buttunPressed(_ sender: Any) {
        print("fff")
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func editPassowrdPressed(_ sender: Any) {
        emailTxtField.isEnabled = true
        phoneTxtField.isEnabled = true
        let SavString = NSLocalizedString("Save", comment: "حفظ")
        EditPersonalProfile.setTitle(SavString, for: .normal)
        if EditPersonalProfile.tag == 2{
            PostProfileInfo()
        }
        EditPersonalProfile.tag = 2
//
    }
    
    var userProfile = userData.Instance
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Profile", comment: "الملف الشخصي") 
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        AddressesTableView.delegate = self
        AddressesTableView.dataSource = self
        userData.Instance.fetchUser()
        loadPersonalData()
        imagePicker.delegate = self
        AlertControllerToGetImage()
//        let gesture =  UITapGestureRecognizer(target: self, action:  #selector(self.chossePhoto(_:)))
        
//        profilePicture.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        loadPersonalData()
    }
    
    
    func AlertControllerToGetImage() {
        alertController = UIAlertController(title: "Get Image", message: "from Gallery or Camera", preferredStyle: UIAlertControllerStyle.alert)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let GalleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(GalleryAction)
        alertController.addAction(cancelAction)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePicture.image = image
            photoChaged = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddressesTableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath)
        if let addressValue = userProfile.data?.addresses?[indexPath.row].address {
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(addressValue)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = AddressesTableView.dequeueReusableCell(withIdentifier: "HeaderAddressCell")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = AddressesTableView.dequeueReusableCell(withIdentifier: "FooterAddressCell")
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAddress = (userProfile.data?.addresses?[indexPath.row]) ?? Addresses()
        performSegue(withIdentifier: "editAddress", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editAddress"{
            let destination = segue.destination as? EditAddressViewController
            destination?.currentAddress = selectedAddress
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.0
    }
    @IBAction func showAddresses(_ sender: Any) {
        if flag == false {
        count = (userProfile.data?.addresses?.count)!
        AddressesTableView.reloadData()
            flag = !flag
        }else{
        count = 0
        AddressesTableView.reloadData()
            flag = !flag
        }
    }
    func loadPersonalData(){
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showProfile() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.userProfile = try JSONDecoder().decode(userData.self, from: response.data!)
                        print(self.userProfile)
                        if let  imageIndex = (self.userProfile.data?.avatar) {
                            self.profilePicture.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
                        }
                        if let  email = (self.userProfile.data?.email) {
                            self.emailTxtField.text = email
                        }
                        if let  phone = (self.userProfile.data?.mobile) {
                            self.phoneTxtField.text = phone
                        }
                        if let  name = (self.userProfile.data?.name) {
                            self.nameTxtField.text = name
                        }
                        self.AddressesTableView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.tag = -1
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    func PostProfileInfo(){
        
        if self.emailTxtField.text == "" || self.phoneTxtField.text == ""{
            let lockString = NSLocalizedString("Please Enter you data", comment: "من فضلك قم بملئ البيانات")
            
            HUD.flash(.label(lockString), delay: 1.0)
            return
        }
//        if self.password1TxtField.text != self.password2TxtField.text {
//            HUD.flash(.label("Your Password Not Identical"), delay: 1.0)
//
//            return
//        }
//
        if self.profilePicture.image == #imageLiteral(resourceName: "images") {
            let lockString = NSLocalizedString("Add Your Profile Picture", comment: "من فضلك قم بإضافة صورة")
            HUD.flash(.label(lockString) , delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        var par = ["email": emailTxtField.text!,"mobile": phoneTxtField.text!  ] as [String : Any]
        if photoChaged {
            par["avatar"] = UIImageJPEGRepresentation(profilePicture.image ?? UIImage(), 0.1)?.base64EncodedString() ?? ""
        }
        
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.updateProfile(), method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            print(response)
            
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                     do {
                    self.userProfile = try JSONDecoder().decode(userData.self, from: response.data!)
                    print(self.userProfile)
                    if let  imageIndex = (self.userProfile.data?.avatar) {
                        self.profilePicture.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
                    }
                    if let  email = (self.userProfile.data?.email) {
                        self.emailTxtField.text = email
                    }
                    if let  phone = (self.userProfile.data?.mobile) {
                        self.phoneTxtField.text = phone
                    }
                    if let  name = (self.userProfile.data?.name) {
                        self.nameTxtField.text = name
                    }
                    self.AddressesTableView.reloadData()
                }catch{
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
}
