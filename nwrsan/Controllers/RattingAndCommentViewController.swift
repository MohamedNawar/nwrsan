//
//  RattingAndCommentViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 9/24/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView

class RattingAndCommentViewController: UIViewController {
    
    var FinishPressed : (()->(Void))?
    
    @IBOutlet weak var comment: UITextView!
    
    @IBOutlet weak var rate: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        comment.layer.cornerRadius = 5
        comment.layer.borderColor = UIColor.gray.cgColor
        comment.layer.borderWidth = 1
    }
    
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func post(_ sender: Any) {
//        postRate()
    }
//    private func postRate(){
//        let header = APIs.sharedInstance.getHeader()
//        let par = ["value": rate.value,
//                   "comment": comment.text!] as [String : Any]
//        print(par)
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.sharedInstance.postRate(id: shop.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
//                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                        //                            user.saveUser(user: user)
//                    }catch{
//                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//                    self.makeDoneAlert(title: "شكرا", SubTitle: "", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
//                    self.FinishPressed?()
//                    self.dismiss(animated: true, completion: nil)
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
