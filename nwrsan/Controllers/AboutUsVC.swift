//
//  AboutUsVC.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import FCAlertView
import Alamofire
import PKHUD

class AboutUsVC: UIViewController {
    var contactUsData = ContactUsModel()

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("About Us", comment: "من نحن")
        lbl.text = NSLocalizedString("About Us", comment: "من نحن")
        titleLbl.text = NSLocalizedString("About Us", comment: "من نحن")
        getContactUs()
        // Do any additional setup after loading the view.
    }

    private func setupUI(){
        titleLbl.text = contactUsData.about?.name
        lbl.text = contactUsData.about?.description
    }
    
    
    func getContactUs(){
        let header = APIs.Instance.getHeader()
        
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.Settings(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.contactUsData = try JSONDecoder().decode(ContactUsModel.self, from: temp3 )
                                self.setupUI()
                            }catch{
                                HUD.hide()
                                HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
                            }
                        }
                    }
                    
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
            
        }
    }
    
    
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
}
