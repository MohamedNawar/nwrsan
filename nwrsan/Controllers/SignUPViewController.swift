//
//  SignUPViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 9/8/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import UIKit
import PKHUD
import SDWebImage
import Alamofire
import FCAlertView
import TextFieldEffects


class SignUPViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , UITextViewDelegate ,UIPickerViewDelegate , UIPickerViewDataSource {
    let CountryPicker = UIPickerView()
    let CityPicker = UIPickerView()
    var selectedCountry = CountryData()
//    var words = [String]()
    
//    var words1 = [String]()
    var CountryIndex = -1
    var cityId :Int = 0
    var photoFalg = false
    @IBOutlet weak var chooseCountryTxtField: HoshiTextField!
    @IBOutlet weak var chooseCityTxtField: HoshiTextField!
    @IBOutlet weak var addressTxtField: HoshiTextField!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var userNameTxtField: HoshiTextField!
    @IBOutlet weak var emailTxtField: HoshiTextField!
    @IBOutlet weak var phoneTxtField: HoshiTextField!
    @IBOutlet weak var password1TxtField: HoshiTextField!
    @IBOutlet weak var password2TxtField: HoshiTextField!
    var alertController = UIAlertController()
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.title = "Sign Up"
        imagePicker.delegate = self
        CountryPicker.delegate = self
        CountryPicker.dataSource = self
        CountryPicker.backgroundColor = .white
        CountryPicker.showsSelectionIndicator = true
        CityPicker.delegate = self
        CityPicker.dataSource = self
        CityPicker.backgroundColor = .white
        CityPicker.showsSelectionIndicator = true
        chooseCityTxtField.isEnabled = false
        AlertControllerToGetImage()
        loadCountryData()
        showCityPicker()
        showCountryPicker()
    }
    func loadCountryData() {
        HUD.show(.progress, onView: self.view)
        CountryMain.Instance.getCountriesServer(enterDoStuff: { () in
//            self.words = CountryMain.Instance.getCountryNameArr()
//            print(self.words)
            self.CountryPicker.reloadAllComponents()
            HUD.hide(animated: true)
        })
    }
    func showCountryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker));

        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain , target: self, action: #selector(cancel))

        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseCountryTxtField.inputView = CountryPicker
        chooseCountryTxtField.inputAccessoryView = toolbar
        
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(done1Picker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseCityTxtField.inputView = CityPicker
        chooseCityTxtField.inputAccessoryView = toolbar
        
    }
    @objc func cancel1(){
        chooseCityTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func cancel(){
        chooseCountryTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if chooseCountryTxtField.text == "" {
            if CountryMain.Instance.countryData.data.count > 0 {
                let arrTemp = CountryMain.Instance.getCountryNameArr()
                chooseCountryTxtField.text = arrTemp.first
                selectedCountry = CountryMain.Instance.countryData.data.first ?? CountryData()
                CountryIndex = 0
                self.CityPicker.reloadAllComponents()
            } else {
                CountryIndex = -1
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        chooseCityTxtField.isEnabled = true
        
    }
 

    @objc func done1Picker(){
        if chooseCityTxtField.text == "" {
            if CountryIndex != -1{
                if (CountryMain.Instance.countryData.data[CountryIndex].cities?.count ?? 0) > 0 {
                    chooseCityTxtField.text = CountryMain.Instance.countryData.data[CountryIndex].cities?.first?.name
                    cityId =  CountryMain.Instance.countryData.data[CountryIndex].cities?.first?.id ?? -1
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
        self.view.endEditing(true)
        
    }
    @IBAction func signUp(_ sender: Any) {
        PostProfileInfo()
    }
    
    @IBAction func addImage(_ sender: Any) {
        present(alertController, animated: true, completion: nil)
    }
    func AlertControllerToGetImage() {
        alertController = UIAlertController(title: "Get Image", message: "from Gallery or Camera", preferredStyle: UIAlertControllerStyle.alert)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let GalleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(GalleryAction)
        alertController.addAction(cancelAction)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePicture.image = image
            photoFalg = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func PostProfileInfo(){
        
        if self.userNameTxtField.text == "" || self.emailTxtField.text == "" || self.password1TxtField.text == "" || self.password2TxtField.text == "" || self.phoneTxtField.text == "" || self.addressTxtField.text == "" || self.chooseCityTxtField.text == "" || self.chooseCountryTxtField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        if self.password1TxtField.text != self.password2TxtField.text {
            HUD.flash(.label("Your Password Not Identical"), delay: 1.0)

            return
        }
        
        
        let header = APIs.Instance.getHeader()
        var par = ["name": userNameTxtField.text!, "email": emailTxtField.text! ,  "address": addressTxtField.text! , "city_id": cityId ,   "mobile": phoneTxtField.text!  , "password": password2TxtField.text!   ] as [String : Any]
        if photoFalg {
            par["avatar"] = UIImageJPEGRepresentation(profilePicture.image ?? UIImage(), 0.1)?.base64EncodedString() ?? ""
        }
        
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            print(response)
            
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        let CartIddTemp = userData.Instance.identifier
                        userData.Instance.remove()
                        
                        //                            UserDefaults.standard.set(response.data!, forKey: "user")
                        userData.Instance.identifier = CartIddTemp
                        UserDefaults.standard.set(CartIddTemp, forKey: "identifier")
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                        navVC.navigationItem.hidesBackButton = true
                        self.navigationController?.pushViewController(navVC, animated: true)
                        HUD.flash(.success, delay: 1.0)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                
                
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == CountryPicker {
            return CountryMain.Instance.countryData.data.count
        }
        if pickerView == CityPicker {
            return CountryMain.Instance.countryData.data[CountryIndex].cities?.count ?? 0
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == CountryPicker {
            return "\(CountryMain.Instance.countryData.data[row].name ?? "")"
            
        }
        if pickerView == CityPicker {
            return "\(CountryMain.Instance.countryData.data[CountryIndex].cities?[row].name ?? "")"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == CountryPicker {
            if CountryMain.Instance.countryData.data.count > 0 {
                CountryIndex = row
                chooseCountryTxtField.text =  CountryMain.Instance.countryData.data[row].name
                self.CityPicker.reloadAllComponents()
                self.CountryPicker.reloadAllComponents()

            }  else{
                CountryIndex = -1
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == CityPicker {
            if CountryIndex != -1{
                if (CountryMain.Instance.countryData.data[CountryIndex].cities?.count ?? 0) > 0 {
                    cityId = CountryMain.Instance.countryData.data[CountryIndex].cities?[row].id ?? -1
                    chooseCityTxtField.text = CountryMain.Instance.countryData.data[CountryIndex].cities?[row].name
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
}

