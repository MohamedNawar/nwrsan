//
//  RootViewController.swift
//  al-fujira
//
//  Created by MACBOOK on 7/17/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tblTableView: UITableView!
    
    var imagesArray = [#imageLiteral(resourceName: "img16"),#imageLiteral(resourceName: "img18"),#imageLiteral(resourceName: "img19"),#imageLiteral(resourceName: "img20"),#imageLiteral(resourceName: "img21")]
    var imagesArray1 = [#imageLiteral(resourceName: "img17"),#imageLiteral(resourceName: "img18"),#imageLiteral(resourceName: "img19"),#imageLiteral(resourceName: "img20"),#imageLiteral(resourceName: "img21"),#imageLiteral(resourceName: "img16")]
    var mainImgArray = [UIImage]()
    var mainArray = [String]()
    
    var nameArray = [
        NSLocalizedString("Login", comment: "تسجيل الدخول"),
        NSLocalizedString("About Us", comment: "من نحن"),
        NSLocalizedString("Contact Us", comment: "تواصل معنا"),
        NSLocalizedString("Share App", comment: "شارك التطبيق"),
        NSLocalizedString("Rules", comment: "الشروط و الأحكام")]
    var nameArray1 = [
        NSLocalizedString("My Requests", comment: "طلباتى"),
        NSLocalizedString("About Us", comment: "من نحن"),
        NSLocalizedString("Contact Us", comment: "تواصل معنا"),
        NSLocalizedString("Share App", comment: "شارك التطبيق"),
        NSLocalizedString("Rules", comment: "الشروط و الأحكام"),
        NSLocalizedString("Logout", comment: "تسجيل الخروج")]
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTableView.delegate = self
        tblTableView.dataSource = self
        tblTableView.separatorStyle = .none
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            mainArray = nameArray
            mainImgArray = imagesArray
        }else{
            mainArray = nameArray1
            mainImgArray = imagesArray1
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if userData.Instance.token == "" || userData.Instance.token == nil {
            mainArray = nameArray
            mainImgArray = imagesArray
        }else{
            mainArray = nameArray1
            mainImgArray = imagesArray1
        }
        tblTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblTableView.dequeueReusableCell(withIdentifier: "SlideMenuCell")!
        
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        
        image.image = mainImgArray[indexPath.row]
        lbl.text = mainArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch mainArray[indexPath.row]{
            
            
        case "My Requests":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case "طلباتى":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case "About Us":
            let vc = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            self.show(vc, sender: nil)
        case "من نحن":
            let vc = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            self.show(vc, sender: nil)
        case "Contact Us":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case "تواصل معنا":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case "Share App":
            let activityController = UIActivityViewController(activityItems: ["image"], applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        case "شارك التطبيق":
            let activityController = UIActivityViewController(activityItems: ["image"], applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        case "Rules":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "RulesVC") as! RulesVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case "الشروط و الأحكام":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "RulesVC") as! RulesVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case "Logout":
            userData.Instance.remove()
            userData.Instance.fetchUser()
            self.dismiss(animated: false, completion: nil)
            if let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarController") {
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        case "تسجيل الخروج":
            userData.Instance.remove()
            userData.Instance.fetchUser()
            self.dismiss(animated: false, completion: nil)
            if let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarController") {
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        case "تسجيل الدخول":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case "Login":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            self.navigationController?.pushViewController(navVC, animated: true)
        default:
            break
        }
    }
    
    
    
}
