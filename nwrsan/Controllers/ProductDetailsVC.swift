//
//  ProductDetailsVC.swift
//  nwrsan
//
//  Created by Moaz Ezz on 9/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class ProductDetailsVC: UIViewController{
    
    @IBOutlet weak var detailsTableView: UITableView!
    var productData2 = ProductsData()
    
    var productData = ProductsData(){
        didSet{
            detailsTableView.reloadData()
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        productData = productData2
        detailsTableView.delegate = self
        detailsTableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductDetailsVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productData.selectable_attributes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let selectableAttributes = productData.selectable_attributes {
                if selectableAttributes[indexPath.row].type == "color" {
                    let cell2 =
                        detailsTableView.dequeueReusableCell(withIdentifier: "DetailsCell2", for: indexPath) as! ColorTableViewCell
                    if let title = selectableAttributes[indexPath.row].name{
                        let name = cell2.viewWithTag(1) as! UILabel
                        name.text = "\(title)"
                    }
                    cell2.productDataContent = selectableAttributes[indexPath.row]
//                    cell2.colorCallBack = {
//                        print("hello")
//                    }
                    return cell2
                }
                if selectableAttributes[indexPath.row].type == "string" {
                    let cell5 =
                        detailsTableView.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                    if let title = selectableAttributes[indexPath.row].name{
                        let name = cell5.viewWithTag(2) as! UILabel
                        name.text = "\(title)"
                    }
                    cell5.productDataContent = selectableAttributes[indexPath.row]
                    cell5.lblCallBack = {
                        (id: Int,flag:Int) -> Void in
                        print("hello")
                    }
                    
                    return cell5
                }
                if selectableAttributes[indexPath.row].type == "number" {
                    let cell22 =
                        detailsTableView.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                    if let title = selectableAttributes[indexPath.row].name{
                        let name = cell22.viewWithTag(2) as! UILabel
                        name.text = "\(title)"
                    }
                    cell22.productDataContent = selectableAttributes[indexPath.row]
                    cell22.lblCallBack = {
                        (id: Int,flag:Int) -> Void in
                        print("hello")
                    }
                    return cell22
                }
                if selectableAttributes[indexPath.row].type == "date" {
                    let cell22 =
                        detailsTableView.dequeueReusableCell(withIdentifier: "DetailsCell22", for: indexPath) as! SecondTableViewCell
                    if let title = selectableAttributes[indexPath.row].name{
                        let name = cell22.viewWithTag(2) as! UILabel
                        name.text = "\(title)"
                    }
                    cell22.productDataContent = selectableAttributes[indexPath.row]
                    cell22.lblCallBack = {
                        (id: Int,flag:Int) -> Void in
                        print("hello")
                    }
                    return cell22
                }
            }
            
        }else if indexPath.section == 1 {
            if (productData.additional_attributes?.count ?? 0) > 0 {
                let additionalAttributes = productData.additional_attributes
                let cell222 =
                    detailsTableView.dequeueReusableCell(withIdentifier: "DetailsCell222", for: indexPath) as! ThirdTableViewCell
                if let title = additionalAttributes?[indexPath.row].name{
                    let name = cell222.viewWithTag(21) as! UILabel
                    name.text = "\(title)"
                }
                cell222.productDataContent = (additionalAttributes?[indexPath.row])!
                return cell222
            }
            
        }
        return UITableViewCell()
    }
    
    
}
