//
//  EditAddressViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import TextFieldEffects
import Font_Awesome_Swift
import PKHUD
import Alamofire
import FCAlertView
class EditAddressViewController: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource  {
    var selectedCountry = CountryData()
    var currentAddress = Addresses()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected1 = ""
    var cityId :Int = 0
    @IBOutlet weak var NameTxtField: HoshiTextField!
    @IBOutlet weak var PhoneNumberTxtField: HoshiTextField!
    @IBOutlet weak var AddressInDetailTxtField: HoshiTextField!
    @IBOutlet weak var isDefult: UISwitch!
    @IBOutlet weak var chooseCityTxtField: HoshiTextField!
    @IBOutlet weak var chooseCountryTxtField: HoshiTextField!
    override func viewDidLoad() {
//        self.navigationController?.navigationItem.title = "Edit Address"
        //new -- this is wrong to set the title
        self.title = NSLocalizedString("Edit Profile", comment: "تعديل الملف الشخصي")
        
        self.title = "تعديل الملف الشخصي"
//        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        super.viewDidLoad()
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker1.delegate = self
        picker1.dataSource = self
        picker1.backgroundColor = .white
        picker1.showsSelectionIndicator = true
            chooseCountryTxtField.text = currentAddress.city?.country?.name
            chooseCityTxtField.text = currentAddress.city?.name
            AddressInDetailTxtField.text = currentAddress.fullname
        NameTxtField.text = currentAddress.name
        PhoneNumberTxtField.text = currentAddress.phone
        cityId = (currentAddress.city?.id)!
        print(cityId)
        
        loadCountryData()
        showCityPicker()
        showCountryPicker()
    }
    func loadCountryData() {
        HUD.show(.progress, onView: self.view)
        CountryMain.Instance.getCountriesServer(enterDoStuff: { () in
            self.words = CountryMain.Instance.getCountryNameArr()
            print(self.words)
            self.picker.reloadAllComponents()
            HUD.hide(animated: true)
        })
    }
    func showCountryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker));
        
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain , target: self, action: #selector(cancel))
        
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseCountryTxtField.inputView = picker
        chooseCountryTxtField.inputAccessoryView = toolbar
        
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(done1Picker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseCityTxtField.inputView = picker1
        chooseCityTxtField.inputAccessoryView = toolbar
        
    }
    @objc func donePicker(){
        if chooseCountryTxtField.text == "" {
            if words.count > 0 {
                chooseCountryTxtField.text = words[0]
                selectedCountry = CountryMain.Instance.countryData.data[0]
                words1 = selectedCountry.getCitiesName()
                self.picker1.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        chooseCityTxtField.isEnabled = true
        
    }
    @objc func cancel(){
        chooseCountryTxtField.text = ""
        self.view.endEditing(true)
    }
    @objc func cancel1(){
        chooseCityTxtField.text = ""
        self.view.endEditing(true)
    }
    
    @objc func done1Picker(){
        if chooseCityTxtField.text == "" {
            if words1.count > 0 {
                chooseCityTxtField.text = words1[0]
                cityId =  CountryMain.Instance.countryData.data[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker {
        return words.count
        }
        if pickerView == picker1 {
            return words1.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker {
         return "\(words[row])"

        }
        if pickerView == picker1 {
            return "\(words1[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker {
            if words.count > 0 {
                selectedCountry = CountryMain.Instance.countryData.data[row]
                words1 = selectedCountry.getCitiesName()
                chooseCountryTxtField.text =  words[row]
                self.picker1.reloadAllComponents()
                self.picker.reloadAllComponents()
                
            }  else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker1 {
            if words1.count > 0 {
                selected1 = words1[row]
                cityId = CountryMain.Instance.countryData.data[row].id ?? 0
                chooseCityTxtField.text = selected1
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func PostAddress(){
        if self.chooseCountryTxtField.text == "" || self.AddressInDetailTxtField.text == "" || self.chooseCityTxtField.text == "" || self.PhoneNumberTxtField.text == "" || self.NameTxtField.text == "" {
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["name": NameTxtField.text!, "city_id": cityId ,"phone": String(describing: PhoneNumberTxtField.text!) , "address": AddressInDetailTxtField.text! , "is_default":isDefult.isOn] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.updateAddress(id: currentAddress.id!), method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            print(response)
            
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func updateAddress(_ sender: Any) {
        PostAddress()
    }
}
