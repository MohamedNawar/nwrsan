//
//  MyOrdersViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class MyOrdersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    var myOrders = MyOrders.Instance
    @IBOutlet weak var OrdersTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        OrdersTableView.delegate = self
        OrdersTableView.dataSource = self
        OrdersTableView.separatorStyle = .none
        loadHomeData()
        userData.Instance.fetchUser()
//        setupView(views: headersubView)
//        setupView(views: footerSubView)
    }
    
    func setupView(views1:UIView){
        views1.layer.shadowColor = UIColor.gray.cgColor
        views1.layer.shadowOpacity = 0.5
        views1.layer.shadowOffset = CGSize.zero
        views1.layer.shadowRadius = 4
        views1.layer.cornerRadius = 10
        views1.clipsToBounds = true
        views1.layer.masksToBounds = false
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == OrdersTableView {
            if let count = myOrders.data?.count {
                return count
            }
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == OrdersTableView {
            if let count = myOrders.data?[section].products?.count   {
                return count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrdersTableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath)
        if let productName = myOrders.data?[indexPath.section].products?[indexPath.row].product?.name {
            let nameLbl = cell.viewWithTag(3) as! UILabel
            nameLbl.text = "\(productName)"
        }
        if let num = myOrders.data?[indexPath.section].products?[indexPath.row].quantity {
            let numLbl = cell.viewWithTag(4) as! UILabel
            numLbl.text = "\(num)"
        }
        if let price = myOrders.data?[indexPath.section].products?[indexPath.row].price {
            let priceLbl = cell.viewWithTag(5) as! UILabel
            priceLbl.text = "\(price)"
        }
        let bacView = cell.viewWithTag(111) as! UIView
        setupView(views1: bacView)

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = OrdersTableView.dequeueReusableCell(withIdentifier: "FooterOrdersCell")
        if let total = myOrders.data?[section].shipping_cost?.amount , let currency = myOrders.data?[section].shipping_cost?.currency {
            let totaLbl = cell?.viewWithTag(7) as! UILabel
            totaLbl.text = "\(total) \(currency)"
        }
        if let location = myOrders.data?[section].address?.name {
            let address = cell?.viewWithTag(6) as! UILabel
            address.text = "\(location)"
        }
        let bacView = cell?.viewWithTag(1112) as! UIView
        setupView(views1: bacView)
        return cell

    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = OrdersTableView.dequeueReusableCell(withIdentifier: "HeaderOrdersCell")
        if let idValue = myOrders.data?[section].id {
            let id = cell?.viewWithTag(1) as! UILabel
            id.text = "#\(idValue)"
        }
        if let dateValue = myOrders.data?[section].status?.updated_at{
            let date = cell?.viewWithTag(2) as! UILabel
            date.text = "\(dateValue)"
        }
        let bacView = cell?.viewWithTag(1113) as! UIView
        setupView(views1: bacView)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 71.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100.0
    }
    func loadHomeData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.ListMyOrders() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myOrders = try JSONDecoder().decode(MyOrders.self, from: response.data!)
                        print(self.myOrders)
                        print("wewewewewewe")
                        print("successsss")
                        if self.myOrders.data?.count == 0  {
                            HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.OrdersTableView)
                        }
                        self.OrdersTableView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
