//
//  MainViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 8/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import AVFoundation
import AVKit
import SideMenu
import YouTubePlayer
import Auk
import moa


struct Cart : Decodable {
    static var Instance = Cart()
    private init() {}
    var count = Int(){
        didSet{
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: count)
        }
    }
}

class MainViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    var collTableViewCell = CollectionsTableViewCell()
    var homeData = HomeData()
    var myCart = MyCart()
    var alertController = UIAlertController()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
//    @IBOutlet weak var youTube: YouTubePlayerView!
    
//    let alertController = UIAlertController(title: "Alert", message: "This is an alert.", preferredStyle: .alert)
//
//    let action1 = UIAlertAction(title: "Default", style: .default) { (action:UIAlertAction) in
//        print("You've pressed default");
//    }
//
//    let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
//        print("You've pressed cancel");
//    }
//
//    let action3 = UIAlertAction(title: "Destructive", style: .destructive) { (action:UIAlertAction) in
//        print("You've pressed the destructive");
//    }
//
//    alertController.addAction(action1)
//    alertController.addAction(action2)
//    alertController.addAction(action3)
//    self.present(alertController, animated: true, completion: nil)
    
    @IBAction func sidePressed(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "en" {
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
            
        }else{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
            
        }

    }
    
    
//    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var TableView: UITableView!
    
    @IBAction func changeLangPressed(_ sender: Any) {
//        let gradePicker = UIPickerView()
//        
//        gradePicker.dataSource = self
//        gradePicker.delegate = self
        
        present(alertController, animated: true, completion: nil)
    }
    
    func AlertControllerToGetImage() {
        alertController = UIAlertController(title: "Set Language", message: "Select your perferd language", preferredStyle: UIAlertControllerStyle.alert)
        let cameraAction = UIAlertAction(title: "العربية", style: UIAlertActionStyle.default) { (action) in
            if L102Language.currentAppleLanguage() == "ar" {
                return
            }
            self.didSelectLang(lang: "ar")
            
        }
        let GalleryAction = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (action) in
            if L102Language.currentAppleLanguage() == "en" {
                return
            }
            self.didSelectLang(lang: "en")
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(GalleryAction)
        alertController.addAction(cancelAction)
        
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.auk.settings.contentMode = .scaleToFill
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
        
        AlertControllerToGetImage()
        
        TableView.separatorStyle = .none
        TableView.allowsSelection = false
        TableView.delegate = self
        TableView.dataSource = self
        loadHomeData()
        setupSideMenu()
        NotificationCenter.default.addObserver(self, selector: #selector(self.chnageCartNum(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        TableView.estimatedRowHeight = 122.0
        TableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    private func setScrollView(){
        if let images = homeData.data?.slider {
            for image in images {
                scrollView.auk.show(url:image )
                
                
            }
        }
    }
    
    @objc private func chnageCartNum(notification: Notification){
        if let obj = notification.object as? Int{
        self.tabBarController?.viewControllers?.last?.tabBarItem.badgeColor = mainColor
        self.tabBarController?.viewControllers?.last?.tabBarItem.badgeValue = "\(obj)"
    }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        print(userData.Instance.token)
//            self.TableView.reloadData()
        self.navigationController?.navigationBar.backgroundColor = mainColor
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
        print(userData.Instance.identifier)
        if userData.Instance.identifier == nil || userData.Instance.identifier == ""{
            loadMyCartData(type: true)
        }
        
        
        
        
        
        if userData.Instance.token == nil || userData.Instance.token == "" {
            if let tabBar =  self.tabBarController{
                if (self.tabBarController?.viewControllers?.count ?? 0) > 3{
                    tabBar.viewControllers?.remove(at:1)  // for 0 index
                    tabBar.viewControllers?.remove(at:2)
                }
                
            }
              // for 1 index
        }else{
            if let tabBar =  self.tabBarController{
                print(tabBar.viewControllers?.count)
                if (tabBar.viewControllers?.count ?? 0) < 4{
                    let navVC1 = UINavigationController()
                    let vc1 = storyboard?.instantiateViewController(withIdentifier: "FavouritesViewController") as! FavouritesViewController
                    navVC1.viewControllers.append(vc1)
                    let customTabBarItem1:UITabBarItem = UITabBarItem(title: nil, image:#imageLiteral(resourceName: "img12"), selectedImage: nil)
                    customTabBarItem1.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                    vc1.tabBarItem = customTabBarItem1
                    tabBar.viewControllers?.insert(navVC1, at: 1)
                    
                    let navVC2 = UINavigationController()
                    let vc2 = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController") as! PersonalProfileViewController
                    let customTabBarItem2:UITabBarItem = UITabBarItem(title: nil, image:#imageLiteral(resourceName: "img14"), selectedImage: nil)
                    customTabBarItem2.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                    customTabBarItem2.titlePositionAdjustment = UIOffset(horizontal: 10, vertical: 10)
                    vc2.tabBarItem = customTabBarItem2
                    navVC2.viewControllers.append(vc2)
                    tabBar.viewControllers?.insert(navVC2, at: 3)
                }
                
            }
        }
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row{
        case 0:
            let cell = TableView.dequeueReusableCell(withIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            cell.categories = (homeData.data?.categories) ?? Categories()
            cell.showAll = { (i) in
                self.mangeShowAll(flag: i)
            }
            cell.PerformSegue = {
                (id: Int,flag:Int) -> Void in
                self.mangeCollections(id: id, flag: flag)
            }
            return cell
        case 1:
            let cell = TableView.dequeueReusableCell(withIdentifier: "OfferCell", for: indexPath) as! OfferCell
            cell.product = (homeData.data?.products) ?? Products()
            cell.PerformSegue = {
                (id: Int,flag:Int) -> Void in
                self.mangeCollections(id: id, flag: flag)
            }
            cell.showAll = { (i) in
                self.mangeShowAll(flag: i)
            }
            return cell
        case 2:
            let cell = TableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath)
            let video = cell.viewWithTag(100) as? YouTubePlayerView
            if let myVideoURL = URL(string: self.homeData.data?.featured_banner?.url ?? "") {
                video?.loadVideoURL(myVideoURL)
            }

            return cell
            
        case 3:
            let cell = TableView.dequeueReusableCell(withIdentifier: "InterBrandsCell", for: indexPath) as! InterBrandsCell
            cell.brand = (homeData.data?.brands) ?? Brands()
            cell.PerformSegue = {
                (id: Int,flag:Int) -> Void in
                self.mangeCollections(id: id, flag: flag)
            }
            cell.showAll = { (i) in
                self.mangeShowAll(flag: i)
            }
            return cell

        default:
            return UITableViewCell()
        }
        
    }
    
    private func mangeCollections(id:Int,flag:Int){//flag= 0 -> colloction1
      
        switch flag {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
            vc.product.id = id
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductsVC") as! ProductsVC
            vc.brand_id = id
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            vc.categoryId = id
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("errot")
        }
        
    }
    
    private func mangeShowAll(flag:Int){//flag= 0 -> colloction1
        
        switch flag {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "SectionsViewController") as! SectionsViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductsVC") as! ProductsVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "AllBrandsViewController") as! AllBrandsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("errot")
        }
        
    }
    
    func loadHomeData(){
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.homePage() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.homeData = try JSONDecoder().decode(HomeData.self, from: response.data!)
                        
                        self.setScrollView()

                        print(self.homeData)
                        print("wewewewewewe")
                        print("successsss")
                        self.TableView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
        }
    
    func loadMyCartData(type:Bool){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.shoppingCartList() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        print(self.myCart)
                        if type {
                            userData.Instance.identifier = self.myCart.data?.identifier
                            UserDefaults.standard.set(self.myCart.data?.identifier, forKey: "identifier")
                            print(userData.Instance.identifier)
                            userData.Instance.fetchUser()
                            print(userData.Instance.identifier)
                            print(userData.Instance.identifier)
                        }
                        
                        print(userData.Instance.identifier)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func didSelectLang(lang: String) {
        print(L102Language.currentAppleLanguage())
        if lang == L102Language.currentAppleLanguage() {
            return
        }
        L102Language.setAppleLAnguageTo(lang: lang)
        if lang == "ar"{
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        };        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    func setupSideMenu(){
        let view = storyboard?.instantiateViewController(withIdentifier: "SideMenuVC")
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: view!)
        let view2 = storyboard?.instantiateViewController(withIdentifier: "SideMenuVC")
        let leftNavigationController = UISideMenuNavigationController(rootViewController: view2!)
        
        
        leftNavigationController.leftSide = true
        SideMenuManager.menuLeftNavigationController = leftNavigationController
        
        menuLeftNavigationController.leftSide = false
        SideMenuManager.menuRightNavigationController = menuLeftNavigationController
    }
}



