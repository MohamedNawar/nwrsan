//
//  ReviewsVC.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView

class ReviewsVC: UIViewController {
    var handleBack: (()->())?
    var comments = [Comments](){
        didSet{
            handleBack?()
            self.tableView.reloadData()
        }
    }
    @IBOutlet weak var commentBtn: UIButton!
    
    @IBAction func commentPressend(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddCommentVC") as! AddCommentVC
        vc.view.tintColor =  mainColor
        vc.transitioningDelegate = self
        vc.modalPresentationStyle = .custom
        vc.handleBack = { () in
            self.loadReviews(id : self.productId)
        }
        vc.productId = productId
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    var productId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadReviews(id : productId)
        self.title = NSLocalizedString("Reviews", comment: "التقييمات")
        commentBtn.setFAIcon(icon: .FAComment , forState: .normal)
        commentBtn.layer.cornerRadius = 30
        // Do any additional setup after loading the view.
    }

    private func loadReviews(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.Rate(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.comments = try JSONDecoder().decode([Comments].self, from: temp3 )
                                if self.comments.count == 0  {
                                    HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.tableView)
                                }
                                
                            }catch{
                                HUD.hide()
                                HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
                            }
                        }
                    }
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    private func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }

}

extension ReviewsVC : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}

class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }
            
            return CGRect(x: 0, y: theView.bounds.height - 388, width: theView.bounds.width, height: 388)
        }
    }
}

extension ReviewsVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath)
        if let  imageIndex = comments[indexPath.row].user?.avatar{
//            print((favoritesData.data?[indexPath.row].images?.first?.large)!)
            let image = cell.viewWithTag(5) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "img06"))
        }
        if let nameValue = comments[indexPath.row].user?.name {
            let name = cell.viewWithTag(4) as! UILabel
            name.text = "\(nameValue)"
        }
        if let date = comments[indexPath.row].created_at_formated {
            let dateLbl = cell.viewWithTag(3) as! UILabel
            dateLbl.text = "\(date)"
        }
        if let comment = comments[indexPath.row].comment {
            let commentLbl = cell.viewWithTag(2) as! UILabel
            commentLbl.text = "\(comment)"
        }
        if let rate = comments[indexPath.row].value {
            let rateLbl = cell.viewWithTag(6) as! HCSStarRatingView
            rateLbl.value = CGFloat(rate)
        }
        return cell
    }
}
