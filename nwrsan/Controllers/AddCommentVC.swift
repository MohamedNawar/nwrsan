//
//  AddCommentVC.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import HCSStarRatingView
import IQKeyboardManagerSwift

class AddCommentVC: UIViewController {
    var handleBack : (()->())?
    var productId = Int()
    @IBOutlet weak var commentText: IQTextView!
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var rate: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl.text = NSLocalizedString("Choose star rate", comment: "اختر تقييمك")
        
        commentText.placeholder = NSLocalizedString("Enter you comment", comment: "ادخل تعليقك")
        // Do any additional setup after loading the view.
    }

    @IBAction func diss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func AddComment(_ sender: Any) {
        if commentText.text == "" {
            let lockString2 = NSLocalizedString("Please Enter you data", comment: "من فضلك قم بملئ البيانات")
            makeDoneAlert(title: lockString2, SubTitle: "", Image: #imageLiteral(resourceName: "Untitled-11"))
            return
        }
        postRate(id: productId)
    }
    
    private func postRate(id:Int){
        print(commentText.text)
        let textTemp = commentText.text
        let header = APIs.Instance.getHeader()
        let par = ["value":Int(rate.value),
                   "comment":textTemp ?? ""] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.Rate(id: id) , method: .post, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    self.handleBack?()
                    self.dismiss(animated: true, completion: nil)
                    //                    do {
                    //                        self.categoriesData = try JSONDecoder().decode(Categories.self, from: response.data!)
                    //                        print(self.categoriesData)
                    //                        self.SectionsCollectionView.reloadData()
                    //                    }catch{
                    //                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    //                        HUD.flash(.label(lockString), delay: 1.0)
                    //                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "Done" , andButtons: nil)
    }
    
}
