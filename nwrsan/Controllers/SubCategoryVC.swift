//
//  BrandsViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class SubCategoryVC: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate  {
    var categoryId = Int(){
        didSet{
            print(categoryId)
            loadCategoriesData(id: categoryId)
            BrandsCollectionView.reloadData()
        }
    }
//    var Id = 0
    var categoriesData = CategoriesSections(){
        didSet{
            self.title = categoriesData.data?.name
            BrandsCollectionView.reloadData()
        }
    }
    @IBOutlet weak var BrandsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.backgroundColor = mainColor
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
//        let textAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)]
//        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        //new -- check this and ask about it
        
        BrandsCollectionView.delegate = self
        BrandsCollectionView.dataSource = self
        print(categoryId)
        print("Any")
        userData.Instance.fetchUser()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let IdTemp = categoriesData.data?.subcategories?[indexPath.row].id  {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductsVC") as! ProductsVC
            vc.subcategory_id = IdTemp
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = categoriesData.data?.subcategories?.count  {
            return count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandsCell", for: indexPath)
        if let  imageIndex = categoriesData.data?.subcategories?[indexPath.row].image {
            print(imageIndex)
            let image = cell.viewWithTag(1) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
        if let nameValue = categoriesData.data?.subcategories?[indexPath.row].name {
            let name = cell.viewWithTag(2) as! UILabel
            name.text = "\(nameValue)"
        }
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shadowRadius = 4
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true
        cell.layer.masksToBounds = false
        return cell
    }
    
    
    func loadCategoriesData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showCategory(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.categoriesData = try JSONDecoder().decode(CategoriesSections.self, from: response.data!)
                        if  self.categoriesData.data?.subcategories?.count == 0  {
                            HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.BrandsCollectionView)
                        }
                        
                        print(self.categoriesData)
                            self.BrandsCollectionView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    
}
extension SubCategoryVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.BrandsCollectionView.frame.width / 2)  , height: 250 )
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}





