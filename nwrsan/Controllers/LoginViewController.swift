//
//  LoginViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
class LoginViewController: UIViewController {

    @IBOutlet weak var password: HoshiTextField!
    @IBOutlet weak var email: HoshiTextField!
    @IBOutlet weak var LoginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
       
    }

    @IBAction func login(_ sender: Any) {
        login()
    }
    func login(){
        if email.text!.isEmpty == false && password.text!.isEmpty == false {
            if !(email.text?.isValidEmail())!{
                makeDoneAlert(title: "Error", SubTitle: "Check if your Email is Valid", Image: #imageLiteral(resourceName: "Untitled-11"))
                return
            }
            let header = APIs.Instance.getHeader()
            let par = ["email": email.text!,
                       "password": password.text!,
                ]
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.login() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                              self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                            let CartIddTemp = userData.Instance.identifier
                            userData.Instance.remove()
                        
//                            UserDefaults.standard.set(response.data!, forKey: "user")
                            userData.Instance.identifier = CartIddTemp
                            UserDefaults.standard.set(CartIddTemp, forKey: "identifier")
                            userData.Instance.saveUser(data: response.data!)
                            userData.Instance.fetchUser()
                            print("wewewewewewe")
                            print("successsss")
//                            if userData.Instance.identifier == "" || userData.Instance.identifier == nil {
//                                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "WayToBayViewController") as! WayToBayViewController
//                                self.navigationController?.pushViewController(navVC, animated: true)
//                            }else{
//                                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
//                                self.navigationController?.pushViewController(navVC, animated: true)
//                            }
                        
                        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                        navVC.navigationItem.hidesBackButton = true
                        self.navigationController?.pushViewController(navVC, animated: true)
                        
                        //new -- check this mohamed
                        
                            
                       
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }else{
            let lockString = NSLocalizedString("Empty field", comment: "حقل فارغ")
            let lockString2 = NSLocalizedString("Please Enter you data", comment: "من فضلك قم بملئ البيانات")
            makeDoneAlert(title: lockString, SubTitle: lockString2, Image: #imageLiteral(resourceName: "Untitled-11"))
            if email.text!.isEmpty == true{
                email.borderActiveColor = UIColor.red
                email.borderInactiveColor = UIColor.red
            }
            if password.text!.isEmpty == true{
                password.borderActiveColor = UIColor.red
                password.borderInactiveColor = UIColor.red
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "Done" , andButtons: nil)
    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    public var length: Int {
        return self.characters.count
    }
}
