//
//  InformationViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 9/11/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import UIKit
import TextFieldEffects
import Font_Awesome_Swift
class InformationViewController: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource  {
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    let words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected1 = ""
    @IBOutlet weak var ArrivalDate: HoshiTextField!
    @IBOutlet weak var governorateTxtField: HoshiTextField!
    @IBOutlet weak var countryTxtField: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker1.delegate = self
        picker1.dataSource = self
        picker1.backgroundColor = .white
        picker1.showsSelectionIndicator = true
        showCountryPicker()
        showgovernoratePicker()
    }
    func showCountryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        countryTxtField.inputView = picker
        countryTxtField.inputAccessoryView = toolbar
        
    }
    func showgovernoratePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(done1Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        governorateTxtField.inputView = picker1
        governorateTxtField.inputAccessoryView = toolbar
        
    }
    
    @objc func donePicker(){
        if countryTxtField.text == "" {
            countryTxtField.text = words[0]
        }
        self.view.endEditing(true)
    }
    @objc func done1Picker(){
        if governorateTxtField.text == "" {
            governorateTxtField.text = words[0]
        }
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker {
            return words.count
        }
        if pickerView == picker1 {
            return words1.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker {
            return "\(words[row])"
            
        }
        if pickerView == picker1 {
            return "\(words1[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker {
            selected = words[row] as! String
            countryTxtField.text = selected
        }
        if pickerView == picker1 {
            selected1 = words1[row] as! String
            governorateTxtField.text = selected1
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
}
