//
//  FavouritesViewController.swift
//  nwrsan
//
//  Created by MACBOOK on 7/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage

class FavouritesViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    var favoritesData = Favorites(){
        didSet{
            FavouritesTableView.reloadData()
        }
    }
    var currentProduct = ProductsData()
    var currentIndex = Int()
    var productsSection = ProductsSection(){
        didSet{
            if let count = favoritesData.data?.count {
            for i in  0..<count {
                if favoritesData.data?[i].id == currentIndex {
                    favoritesData.data?.remove(at: i)
                    FavouritesTableView.reloadData()
                }
            }
            }
        }
    }
    var myCart = MyCart()
    @IBOutlet weak var FavouritesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationItem.title = "My Favourites"
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        
        FavouritesTableView.dataSource = self
        FavouritesTableView.delegate = self
        FavouritesTableView.separatorStyle = .none
        userData.Instance.fetchUser()
        loadFavoritesData()
    }
    override func viewWillAppear(_ animated: Bool) {
        loadFavoritesData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == FavouritesTableView {
            if let count = favoritesData.data?.count  {
                return count
            }
        }
        return 0
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FavouritesTableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        cell.updateViews(product: (favoritesData.data![indexPath.row]))
        currentProduct = (favoritesData.data?[indexPath.row])!
        
        cell.favoriteCallBack = {
            () -> () in
            self.addToFavorite(productID: self.favoritesData.data?[indexPath.row].id ?? -1)
            self.currentIndex = (self.favoritesData.data?[indexPath.row].id)!
                        self.favoritesData.data?.remove(at: indexPath.row)
                        self.FavouritesTableView.reloadData()
            
        }
        cell.cartCallBack = {
            () -> () in
            self.mangeAddToCart()
        }
        return cell
        
    }
    private func mangeAddToCart(){
        let id = currentProduct.id
        let qty = currentProduct.qty
        addToCart(productID: id!, qty: qty!)
    }

    func loadFavoritesData(){
        print("ToDo -> add flah for hud")
        let header = APIs.Instance.getHeader() 
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showFavorites() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.favoritesData = try JSONDecoder().decode(Favorites.self, from: response.data!)
                        if  self.favoritesData.data?.count == 0  {
                            HUD.show(.label(NSLocalizedString("No items Found", comment: "لا يوجد منتجات")), onView: self.FavouritesTableView)
                        }
                        print(self.favoritesData)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func addToFavorite(productID:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddToFavorites(id: productID), method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                        self.productsSection = try JSONDecoder().decode(ProductsSection.self, from: response.data!)
                        HUD.flash(.success, delay: 1.0)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func addToCart(productID:Int , qty:Int){
        if currentProduct.selectable_attributes?.count != 0 {
            performSegue(withIdentifier: "showProduct1", sender: self)
        }
        let par = ["product_id": productID , "qty": qty ] as [String : Any]
        
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addProductToShoppingCart(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                         self.myCart = try JSONDecoder().decode(MyCart.self, from: response.data!)
                        self.FavouritesTableView.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }

}
