//
//  DonePopUp.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class DonePopUp: UIViewController {
    var handleBack : (()->())?
    
    @IBOutlet weak var secLbl: UILabel!
    @IBOutlet weak var mainLbl: UILabel!
    @IBAction func donePressed(_ sender: UIButton) {
        diss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
            mainLbl.text = NSLocalizedString("Thanks", comment: "شكرا")
        secLbl.text = NSLocalizedString("You have successfully completed your purchase", comment: "لقد تم الطلب بنجاح")
                self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.diss)))
        // Do any additional setup after loading the view.
    }

    @objc private func diss(){
        self.dismiss(animated: true, completion: {
            self.handleBack?()
        })
    }

}
