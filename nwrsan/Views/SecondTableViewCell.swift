//
//  SecondTableViewCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import PKHUD
import FCAlertView

class SecondTableViewCell: UITableViewCell, UICollectionViewDataSource , UICollectionViewDelegate {
    var lblCallBack: ((Int , Int) -> Void)?
    
    @IBOutlet weak var lableCollectionView: UICollectionView!
    @IBOutlet weak var titleLable: UILabel!
    var productDataContent = Attributes(){
        didSet{
            lableCollectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        lableCollectionView.delegate = self
        lableCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = productDataContent.values?.count {
            return count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectLabelCell", for: indexPath)
        if let valueOfName = productDataContent.values?[indexPath.row].value?.SString
        {
            let img = cell.viewWithTag(-2) as! UILabel
            img.text = valueOfName
            
        }else{
            if let valueOfName = productDataContent.values?[indexPath.row].value?.IInt
            {
                let img = cell.viewWithTag(-2) as! UILabel
                img.text = "\(valueOfName)"
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        lableCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
        if productDataContent.type == "string" {
            lblCallBack!(indexPath.row ,1)
        }else if productDataContent.type == "number" {
            lblCallBack!(indexPath.row ,2)
        }else if productDataContent.type == "date" {
            lblCallBack!(indexPath.row ,3)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        lableCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
    }
    
}




