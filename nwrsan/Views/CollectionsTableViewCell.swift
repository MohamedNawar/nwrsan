//
//  CollectionsTableViewCell.swift
//  nwrsan
//
//  Created by MACBOOK on 8/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import SDWebImage
import YouTubePlayer
class CollectionsTableViewCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource  {
    
    var PerformSegue: ((Int , Int) -> Void)?
    var segueId = Int()
    var videUrl = ""{
        didSet{
            BestOfferCollection.reloadData()
        }
    }
    var product = Products(){
        didSet{
            BestOfferCollection.reloadData()
        }
    }
    var brand = Brands(){
        didSet{
            BrandsCollections.reloadData()
        }
    }
    var categories = Categories(){
        didSet{
            SectionsCollections.reloadData()
        }
    }
    @IBOutlet weak var SectionsCollections: UICollectionView!
    @IBOutlet weak var BrandsCollections: UICollectionView!
    @IBOutlet weak var BestOfferCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        SectionsCollections.delegate = self
        SectionsCollections.dataSource = self
        BrandsCollections.delegate = self
        BrandsCollections.dataSource = self
        BestOfferCollection.delegate = self
        BestOfferCollection.dataSource = self
        if let layout = SectionsCollections?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }

        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == self.BestOfferCollection {
            return product.data?.count ?? 0
        }
        
        if collectionView == self.BrandsCollections {
            return brand.data?.count ?? 0
        }
        if collectionView == self.SectionsCollections {
            return 3
        }

        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:UICollectionViewCell?
        
        if collectionView == self.BestOfferCollection {
            let cell = BestOfferCollection.dequeueReusableCell(withReuseIdentifier: "OffersCell", for: indexPath)
            if let  imageIndex = (product.data?[indexPath.row].images?.first?.large) {
            let image = cell.viewWithTag(1) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }
            if let sale = product.data?[indexPath.row].discount_percentage {
                let saleLabel = cell.viewWithTag(4) as! UILabel
                if sale > 0 {
                    saleLabel.text = "\(sale)% " + NSLocalizedString("Sale", comment: "خصم")
                }else{
                    saleLabel.isHidden = true
                }
            }
            if let nameValue = product.data?[indexPath.row].name {
            let name = cell.viewWithTag(2) as! UILabel
            name.text = "\(nameValue)"
            }
            if let quantityValue = product.data?[indexPath.row].qty {
                let quantity = cell.viewWithTag(3) as! UILabel
                quantity.text = "\(quantityValue)"
            }

            return cell
        }
        
        if collectionView == self.BrandsCollections {
            cell = BrandsCollections.dequeueReusableCell(withReuseIdentifier: "BrandCell", for: indexPath)
            if let  imageIndex = (brand.data?[indexPath.row].logo) {
                let image = cell?.viewWithTag(5) as! UIImageView
                image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }

            return cell!
        }
        if collectionView == self.SectionsCollections {
            cell = SectionsCollections.dequeueReusableCell(withReuseIdentifier: "SectionCell", for: indexPath)
            if let  imageIndex = (categories.data?[indexPath.row].image) {
                let image = cell?.viewWithTag(6) as! UIImageView
                image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }
            if let nameValue = categories.data?[indexPath.row].name {
                let name = cell?.viewWithTag(7) as! UILabel
                name.text = "\(nameValue)"
            }

            return cell!
        }
        
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.BestOfferCollection {
            switch kind {
                
            case UICollectionElementKindSectionHeader:
                
                sectionHeaderView = BestOfferCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "OffersCellHeaderView", for: indexPath)
//                    let view = sectionHeaderView.viewWithTag(1) as!  YouTubePlayerView
//                if let myVideoURL = URL(string: videUrl) {
//                view.loadVideoURL(myVideoURL)
//                }

//                print(videUrl)
                return sectionHeaderView

            case UICollectionElementKindSectionFooter:
                let footerView = BestOfferCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "BrandCellHeaderView", for: indexPath)
                return footerView
                
            default:
                assert(false, "Unexpected element kind")
            }

        }
        
        
        return sectionHeaderView
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == BestOfferCollection {
            segueId = (product.data?[indexPath.row].id)!
            PerformSegue!(segueId, 0)
        }
        if collectionView == BrandsCollections {
            segueId = (brand.data?[indexPath.row].id)!
            PerformSegue!(segueId, 1)
        }
        if collectionView == SectionsCollections {
            segueId = (categories.data?[indexPath.row].id)!
            PerformSegue!(segueId, 2)
        }
    }
    
    
}
extension CollectionsTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.BestOfferCollection {
            return CGSize(width: (self.BestOfferCollection.frame.width / 2) - 10  , height: 130 )
        }
        if collectionView == self.BrandsCollections {
            return CGSize(width: 150 , height: 170 )
        }
        if collectionView == self.SectionsCollections {
            return CGSize(width: (self.BestOfferCollection.frame.width / 2) - 10  , height: 120 )
        }
        return CGSize(width: 50 , height: 120 )

    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
extension CollectionsTableViewCell: PinterestLayoutDelegate {
    func collectionView(_ collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat{
        if collectionView == self.SectionsCollections {
            if indexPath.row == 0 {
                return 150
            }
            if indexPath.row == 1 {
                return 312
            }
            if indexPath.row == 2 {
                return 150
            }

        }
       return 100
    }

}

