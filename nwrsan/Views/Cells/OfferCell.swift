//
//  OfferCell.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class OfferCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource  {
    
    var PerformSegue: ((Int , Int) -> Void)?
    var segueId = Int()
    
    var product = Products(){
        didSet{
            BestOfferCollection.reloadData()
        }
    }
    
    var showAll: ((Int) -> Void)?
    @IBAction func showAllPressed(_ sender: Any) {
        showAll?(1)
    }
    @IBOutlet weak var BestOfferCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        BestOfferCollection.delegate = self
        BestOfferCollection.dataSource = self

        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return product.data?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = BestOfferCollection.dequeueReusableCell(withReuseIdentifier: "OffersCell", for: indexPath)
        if let  imageIndex = (product.data?[indexPath.row].images?.first?.large) {
            let image = cell.viewWithTag(1) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
        if let sale = product.data?[indexPath.row].discount_percentage {
            let saleLabel = cell.viewWithTag(4) as! UILabel
            if sale > 0 {
                saleLabel.text = "\(sale)% " + NSLocalizedString("Sale", comment: "خصم")
            }else{
                saleLabel.isHidden = true
            }
        }
        if let nameValue = product.data?[indexPath.row].name {
            let name = cell.viewWithTag(2) as! UILabel
            name.text = "\(nameValue)"
        }
        if let quantityValue = product.data?[indexPath.row].qty {
            let quantity = cell.viewWithTag(3) as! UILabel
            quantity.text = ""
        }
        
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        segueId = (product.data?[indexPath.row].id)!
        PerformSegue!(segueId, 0)
    }
    
    
}
extension OfferCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.BestOfferCollection.frame.width / 2) - 10  , height: 130 )
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

