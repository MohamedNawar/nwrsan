//
//  OfferCell.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource  {
    
    var PerformSegue: ((Int , Int) -> Void)?
    var segueId = Int()
    
    var categories = Categories(){
        didSet{
            BestOfferCollection.reloadData()
        }
    }
    
    var showAll: ((Int) -> Void)?
    @IBAction func showAllPressed(_ sender: Any) {
        showAll?(0)
    }
    @IBOutlet weak var showAllOutlet: UIButton!
    
    @IBOutlet weak var BestOfferCollection: UICollectionView!
    
    @IBOutlet weak var Cateogrieslbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        BestOfferCollection.delegate = self
        BestOfferCollection.dataSource = self
       Cateogrieslbl.text = NSLocalizedString("Categories", comment: "الأقسام")
        showAllOutlet.setTitle(NSLocalizedString("Show All", comment: "شاهد المزيد"), for: .normal)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (categories.data?.count ?? 0) > 5{
            return 6
        }
        return categories.data?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = BestOfferCollection.dequeueReusableCell(withReuseIdentifier: "OffersCell", for: indexPath)
        if let  imageIndex = (categories.data?[indexPath.row].image) {
            let image = cell.viewWithTag(1) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
//        if let sale = categories.data?[indexPath.row].discount_percentage {
//            let saleLabel = cell.viewWithTag(4) as! UILabel
//            if sale > 0 {
//                saleLabel.text = "\(sale)% " + NSLocalizedString("Sale", comment: "خصم")
//            }else{
//                saleLabel.isHidden = true
//            }
//        }
        if let nameValue = categories.data?[indexPath.row].name {
            let name = cell.viewWithTag(-1) as! UILabel
            name.text = "\(nameValue)"
        }
//        if let quantityValue = categories.data?[indexPath.row].qty {
//            let quantity = cell.viewWithTag(3) as! UILabel
//            quantity.text = ""
//        }
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        segueId = (categories.data?[indexPath.row].id)!
        PerformSegue!(segueId, 2)
    }
    
    
}
extension CategoriesCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.BestOfferCollection.frame.width / 3) - 10  , height: 130 )
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

