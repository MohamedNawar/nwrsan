//
//  ColorTableViewCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

    import UIKit
    import Alamofire
    import SDWebImage
    import PKHUD
    import FCAlertView
    
    class ColorTableViewCell: UITableViewCell, UICollectionViewDataSource , UICollectionViewDelegate {
         var colorCallBack: ((Int , Int) -> Void)?
        @IBOutlet weak var colorCollectionView: UICollectionView!
        var productDataContent = Attributes(){
            didSet{
                colorCollectionView.reloadData()
            }
        }
        override func awakeFromNib() {
            super.awakeFromNib()
            colorCollectionView.delegate = self
            colorCollectionView.dataSource = self
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if let count = productDataContent.values?.count {
                return count
            }
            return 0
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            var cell = UICollectionViewCell()
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectColorCell", for: indexPath)
            if let valueOfName = productDataContent.values?[indexPath.row].value?.SString
            {
                 let img = cell.viewWithTag(32) as! UIImageView
                img.cornerRadius = 25
                img.sd_setImage(with: URL(string: valueOfName  ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
                
            }
            return cell
        }
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            colorCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
//            if productDataContent.name == "لون" {
//                colorCallBack!(indexPath.row ,1)
//            }else if productDataContent.name == "لون تجريبي" {
//                colorCallBack!(indexPath.row ,2)
//            }
            colorCallBack?(indexPath.row ,2)
        }
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            colorCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
            colorCollectionView.cellForItem(at: indexPath)?.reloadInputViews()
        }
        
}




