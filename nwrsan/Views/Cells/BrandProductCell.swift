//
//  BrandProductCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import UIKit
import PKHUD
import SDWebImage
import Alamofire
import FCAlertView
import TextFieldEffects

class BrandProductCell: UICollectionViewCell {
    @IBOutlet weak var saleLable: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var catBtn: UIButton!
    @IBOutlet weak var saleImage: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productNewPrice: UILabel!
    @IBOutlet weak var productOldPrice: UILabel!
    var favoriteCallBack: (() -> Void)?
    var cartCallBack: (() -> Void)?
    var currentProduct = ProductsData(){
        didSet{
        if currentProduct.is_favorite?.BBool == true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
        }else{
            favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
        }
            self.reloadInputViews()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func updateViews(product: ProductsData) {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 4
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        currentProduct = product
        if let  imageIndex = (product.images?.first?.large) {
            productImage.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
        productTitle.text = product.name
        
        
        
        if let sale = product.discount_percentage {
            if sale > 0 {
                saleLable.text = "\(sale)% " + NSLocalizedString("Sale", comment: "خصم")
            }else{
                saleImage.isHidden = true
                saleLable.isHidden = true
            }
        }
        
        if let priceValue = product.new_price?.amount , let currency = product.new_price?.currency {
            productNewPrice.text = "\(priceValue) \(currency)"
        }
        if let oldPrice = product.old_price?.amount , let currency = product.old_price?.currency {
            let oldPriceString:String = String(oldPrice) + " " + currency
            let textRange = NSMakeRange(0, oldPriceString.count)
            let attributedText = NSMutableAttributedString(string: oldPriceString)
            attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                        value: NSUnderlineStyle.styleSingle.rawValue,
                                        range: textRange)
            productOldPrice.attributedText = attributedText
        }
        if currentProduct.is_favorite?.BBool == true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "ic_like_1"), for: .normal)
        }else{
            favoriteBtn.setImage(#imageLiteral(resourceName: "img37"), for: .normal)
        }
       
    }
    
    @IBAction func favoriteBtn(_ sender: Any) {
        favoriteCallBack!()
           }
    @IBAction func cartBtn(_ sender: Any) {
        cartCallBack!()
        
    }
}
