//
//  FavouriteCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/18/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class FavouriteCell: UITableViewCell {

    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    var favoriteCallBack: (() -> Void)?
    var cartCallBack: (() -> Void)?
    var currentProduct = ProductsData()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func removeFromFavourite(_ sender: Any) {
        favoriteCallBack!()

    }
    
    
    @IBAction func addToCart(_ sender: Any) {
        cartCallBack!()

    }
    func updateViews(product: ProductsData) {
        currentProduct = product
        if let  imageIndex = (currentProduct.images?.first?.large) {
            imgView.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
        productNameLbl.text = product.name
        if let priceValue = product.new_price?.amount , let currency = product.new_price?.currency {
            priceLbl.text = "\(priceValue) \(currency)"
        }
        if let oldPrice = product.old_price?.amount , let currency = product.old_price?.currency {
            let oldPriceString:String = String(oldPrice) + " " + currency
            let textRange = NSMakeRange(0, oldPriceString.count)
            let attributedText = NSMutableAttributedString(string: oldPriceString)
            attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                        value: NSUnderlineStyle.styleSingle.rawValue,
                                        range: textRange)
            oldPriceLbl.attributedText = attributedText
        }
    }
    
}
//if let  imageIndex = favoritesData.data?[indexPath.row].images?.first?.large {
//    print((favoritesData.data?[indexPath.row].images?.first?.large)!)
//    let image = cell.viewWithTag(1) as! UIImageView
//    image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "img06"))
//}
//if let nameValue = favoritesData.data?[indexPath.row].name {
//    let name = cell.viewWithTag(2) as! UILabel
//    name.text = "\(nameValue)"
//}
//if let oldPrice = favoritesData.data?[indexPath.row].old_price?.amount_formated {
//    var oldPriceLbl = cell.viewWithTag(4) as! UILabel
//    let textRange = NSMakeRange(0, oldPrice.count)
//    let attributedText = NSMutableAttributedString(string: oldPrice)
//    attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
//                                value: NSUnderlineStyle.styleSingle.rawValue,
//                                range: textRange)
//    oldPriceLbl.attributedText = attributedText
//
//}
//if let newPrice = favoritesData.data?[indexPath.row].new_price?.amount_formated {
//    let newPriceLbl = cell.viewWithTag(3) as! UILabel
//    newPriceLbl.text = "\(newPrice)"
//}
