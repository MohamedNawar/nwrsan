//
//  CartTableViewCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/19/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import Alamofire
import FCAlertView
//import TextFieldEffects
class CartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var oldPrice: UILabel!
    var quentityChanged:(()-> Void)!
    var quentity = Int(){
        didSet{
            numberOfOrder.text = "\(quentity)"
            quentityChanged()
        }
    }
        var cartCallBack: (() -> Void)?
        var currentProduct = MyCartDetailsItems()
        @IBOutlet weak var productImage: UIImageView!
        @IBOutlet weak var productTitle: UILabel!
        @IBOutlet weak var productNewPrice: UILabel!
        @IBOutlet weak var numberOfOrder: UILabel!
    
    @IBOutlet weak var optionsLbl: UILabel!
    @IBOutlet weak var haveAttributes: UIStackView!
    
    
        func updateViews(product:MyCartDetailsItems) {
            optionsLbl.text = NSLocalizedString("more Info", comment: "الخصائص")
             currentProduct = product
            numberOfOrder.text = "\(product.qty ?? -1)"
            let url = URL(string: product.item?.image ?? "")
            productImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            productTitle.text = product.item?.name
            if let newPrice = product.total?.amount , let currency = product.total?.currency {
                productNewPrice.text = "\(newPrice) \(currency)"
            }
            if let OldPrice = (product.old_total?.amount) , let currency = product.old_total?.currency{
                let OldPriceString:String = String(OldPrice) + " " + currency
                let textRange = NSMakeRange(0, OldPriceString.count)
                let attributedText = NSMutableAttributedString(string: OldPriceString)
                attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                            value: NSUnderlineStyle.styleSingle.rawValue,
                                            range: textRange)
                oldPrice.attributedText = attributedText
            }
            
            if (product.item?.customAttributes?.count ?? 0) < 1{
                haveAttributes.isHidden = true
            }
        }
        
    @IBAction func addOne(_ sender: Any) {
        let temp = currentProduct.qty ?? -1
        quentity = temp + 1
        
    }
    @IBAction func removeFromCart(_ sender: Any) {
        cartCallBack!()
    }
    @IBAction func removeOne(_ sender: Any) {
        if (currentProduct.qty ?? 1) > 1 {
            let temp = currentProduct.qty ?? -1
            quentity = temp - 1
        }
        
    }
}
