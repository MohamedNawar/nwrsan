//
//  InterBrandsCell.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class InterBrandsCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource {
    
    var PerformSegue: ((Int , Int) -> Void)?
    var segueId = Int()
    
    var brand = Brands(){
        didSet{
            BrandsCollections.reloadData()
        }
    }
    var showAll: ((Int) -> Void)?
    @IBAction func showAllPressed(_ sender: Any) {
        showAll?(2)
    }
    
    @IBOutlet weak var BrandsCollections: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        BrandsCollections.delegate = self
        BrandsCollections.dataSource = self
  
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return brand.data?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = BrandsCollections.dequeueReusableCell(withReuseIdentifier: "BrandCell", for: indexPath)
        if let  imageIndex = (brand.data?[indexPath.row].logo) {
            let image = cell.viewWithTag(5) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        segueId = (brand.data?[indexPath.row].id)!
        PerformSegue!(segueId, 1)
    }
    
    
}
extension InterBrandsCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150 , height: 170 )
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

