//
//  ThirdTableViewCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

    import UIKit
    import Alamofire
    import SDWebImage
    import PKHUD
    import FCAlertView
    
    class ThirdTableViewCell: UITableViewCell, UICollectionViewDataSource , UICollectionViewDelegate {
        
        @IBOutlet weak var additionalCollectionView: UICollectionView!
        var productDataContent = Attributes(){
            didSet{
                additionalCollectionView.reloadData()
                additionalCollectionView.allowsSelection = false
            }
        }
        override func awakeFromNib() {
            super.awakeFromNib()
            additionalCollectionView.delegate = self
            additionalCollectionView.dataSource = self
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            var cell = UICollectionViewCell()
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nonSelectCell", for: indexPath)
            if let valueOfName = productDataContent.value
            {
                let img = cell.viewWithTag(32) as! UIImageView
                img.cornerRadius = 25
                img.sd_setImage(with: URL(string: valueOfName  ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
                
            }
            return cell
        }
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        }
        
}

