//
//  OffersCCell.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class CategoriesCCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource {
    var segueId = Int()
    
    var PerformSegue: ((Int , Int) -> Void)?
    var categories = Categories(){
        didSet{
            CategoriesCollections.reloadData()
        }
    }
    var showAll: ((Int) -> Void)?
    @IBAction func showAllPressed(_ sender: Any) {
        showAll?(0)
    }
    @IBOutlet weak var CategoriesCollections: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        CategoriesCollections.delegate = self
        CategoriesCollections.dataSource = self
        
        if let layout = CategoriesCollections?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
         let cell = CategoriesCollections.dequeueReusableCell(withReuseIdentifier: "SectionCell", for: indexPath)
            if let  imageIndex = (categories.data?[indexPath.row].image) {
                let image = cell.viewWithTag(6) as! UIImageView
                image.sd_setImage(with: URL(string: imageIndex ), placeholderImage: #imageLiteral(resourceName: "dummyImg"))
            }
            if let nameValue = categories.data?[indexPath.row].name {
                let name = cell.viewWithTag(7) as! UILabel
                name.text = "\(nameValue)"
            }
            
        return cell
        
        
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == CategoriesCollections {
            segueId = (categories.data?[indexPath.row].id)!
            PerformSegue!(segueId, 2)
        }
    }
}

extension CategoriesCCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.CategoriesCollections.frame.width / 2) - 10  , height: 120 )
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
extension CategoriesCCell: PinterestLayoutDelegate {
    func collectionView(_ collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat{
        if collectionView == self.CategoriesCollections {
            if indexPath.row == 0 {
                return 150
            }
            if indexPath.row == 1 {
                return 312
            }
            if indexPath.row == 2 {
                return 150
            }
            
        }
        return 100
    }
    
}
