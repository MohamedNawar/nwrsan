//
//  RattingCell.swift
//  nwrsan
//
//  Created by MACBOOK on 9/24/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import HCSStarRatingView
class RattingCell: UITableViewCell {
    var ratting = Rating(){
        didSet{
            if let value = ratting.rating_average {
                rattingValue.text = "\(value)"
                 rate.value = CGFloat(value)
            }
            if let count = ratting.rating_count {
                if count == 0 {
                    rattingCount.text = NSLocalizedString("No rating", comment: "لا يوجد تقييم")
                }else{
                   rattingCount.text = "\(count)"
                }
                
            }
           
        }
    }
    @IBOutlet weak var rattingValue: UILabel!
    @IBOutlet weak var rattingCount: UILabel!
    @IBOutlet weak var rate: HCSStarRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
