//
//  LoginPopUp.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class LoginPopUp: UIViewController {
    var handleBack : ((Bool)->())?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.diss)))
        // Do any additional setup after loading the view.
    }
    
    @objc private func diss(){
        self.dismiss(animated: true, completion: {
//            self.handleBack?()
        })
    }

    
    @IBAction func createAccount(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            self.handleBack?(false)
        })
    }
    @IBAction func login(_ sender: Any) {
        self.dismiss(animated: true, completion: {
        self.handleBack?(true)
        })
    }
    
}
