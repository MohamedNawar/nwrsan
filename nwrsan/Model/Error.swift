//
//  ErrorHandler.swift
//  Rafeeq
//
//  Created by Mezoo on 8/27/18.
//  Copyright © 2018 Mezoo. All rights reserved.
//

import Foundation

class ErrorHandler : Decodable{
    var message : String?
    var errors : ErrorTypes?
    func parseError() -> String {
        var str = ""
        str.append(message ?? "")
        if let temp = self.errors?.email{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.name{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.password{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.avatar{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.address{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.city_id{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.mobile{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")

            }
        }
        if let temp = self.errors?.customAttributes{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.qty{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        
        if let temp = self.errors?.content{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.rate{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        return str
    }
    
    
}
struct ErrorImd : Decodable {
    var errors : ErrorTypes?
}
struct ErrorTypes : Decodable {
    var email : [String]?
    var password : [String]?
    var name : [String]?
    var avatar : [String]?
    var city_id : [String]?
    var address : [String]?
    var mobile:[String]?
    var qty:[String]?
    var customAttributes:[String]?
    var content:[String]?
    var rate:[String]?
    
    
    
}
