//
//  APIs.swift
//  nwrsan
//
//  Created by Mohamed Nawar on 9/8/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}
    
    private let url = "http://nwrsan.com"
    
    public func getHeader() -> [String: String]{
        print((userData.Instance.identifier))
        let header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \(userData.Instance.token ?? "")" , "Cart-Identifier" : "\(userData.Instance.identifier ?? "")",
            "X-Accept-Language":L102Language.currentAppleLanguage()
        ]
        return header
    }
    
    public func registeration() -> String{
        return url + "/api/auth/register"
    }
    
    public func login() -> String{
        return url + "/api/auth/login"
    }
    public func showProfile() -> String{
        return url + "/api/profile"
    }
    public func updateProfile() -> String{
        return url + "/api/profile"
    }
    public func addAddress() -> String{
        return url + "/api/addresses"
    }
    public func updateAddress(id : Int) -> String{
        return url + "/api/addresses/\(id)"
    }
    public func listingBrands() -> String{
        return url + "/api/brands"
    }
    public func listingProducts() -> String{
        return url + "/api/products"
    }
    public func showProduct(id : Int) -> String{
        return url + "/api/products/\(id)"
    }
    public func Rate(id : Int) -> String{
        return url + "/api/products/\(id)/rates"
    }
    public func listingCategories() -> String{
        return url + "/api/categories"
    }
    public func showCategory(id : Int) -> String{
        return url + "/api/categories/\(id)"
    }
    public func homePage() -> String{
        print(url + "/api/home")
        return url + "/api/home"
    }
    public func Settings() -> String{
        return url + "/api/settings"
    }
    public func contactMessages() -> String{
        return url + "/api/contact_messages"
    }
    public func listingCurrencies() -> String{
        return url + "/api/currencies"
    }
    public func listingCountries() -> String{
        return url + "/api/countries"
    }
    public func shoppingCartList() -> String{
        print(url + "/api/shopping_cart_products")
        return url + "/api/shopping_cart_products"
    }
    public func addProductToShoppingCart() -> String{
        return url + "/api/shopping_cart_products"
    }
    public func UpdateItemInShoppingCart(id : Int) -> String{
        print(url + "/api/shopping_cart_products/\(id)")
        return url + "/api/shopping_cart_products/\(id)"
    }
    public func DeleteItemInShoppingCart(id : Int) -> String{
        return url + "/api/shopping_cart_products/\(id)"
    }
    public func addCoupon() -> String{
        return url + "/api/add-coupon"
    }
    public func Checkout() -> String{
        return url + "/api/checkout"
    }
    public func MyAddresses() -> String{
        return url + "/api/addresses"
    }
    
    public func showFavorites() -> String{
        return url + "/api/favorites"
    }
    
    public func AddToFavorites(id : Int) -> String{
        print(url + "/api/products/\(id)/favorites")
        return url + "/api/products/\(id)/favorites"
    }
    
    
    public func ListMyOrders() -> String{
        return url + "/api/orders"
    }
    
    public func ShowOrder(id : Int) -> String{
        return url + "/api/orders/\(id)"
    }
    
//    public func updateProfile() -> String{
//        return url + "/api/profile"
//    }
//
//    public func getMyCards() -> String{
//        return url + "/api/cards/me"
//    }
//
//    public func getContactUs() -> String{
//        return url + "/api/contact_us"
//    }
//
//    public func addCardToContacts(id : Int) -> String{
//        return url + "/api/cards/contacts/\(id)"
//    }
//
//    public func removeCardFromContacts(id : Int) -> String{
//        return url + "/api/cards/contacts/\(id)"
//    }
//
//    public func storeCard() -> String{
//        return url + "/api/cards"
//    }
//
//    public func updateCard(id : Int) -> String{
//        return url + "/api/cards/\(id)"
//    }
//
//    public func getShowCard(id : Int) -> String{
//        return url + "/api/cards/\(id)"
//    }
//
//    public func deleteCard(id : Int) -> String{
//        return url + "/api/cards/\(id)"
//    }
//
//    public func getNearbyCards() -> String{
//        return url + "/api/cards/nearby"
//    }
//
//    public func getSearchForCard() -> String{
//        return url + "/api/cards/search"
//    }
//
//    public func getRandomCard() -> String{
//        return url + "/api/cards/random"
//    }
//    
//    public func getFilterCards() -> String{
//        return url + "/api/cards/filters"
//    }
//
//    public func settings() -> String{
//        return url + "/api/settings"
//    }
    
}

////
////  UserData.swift
////  skillzy
////
////  Created by Mohamed Nawar on 8/28/18.
////  Copyright © 2018 MohamedHassanNawar. All rights reserved.
////
//
//import Foundation
//
//class User {
//    static let Instance = User()
//    private init() {}
//    var user = userData()
//    func saveUser() {
//        UserDefaults.standard.set(user.token, forKey: "user")
//        UserDefaults.standard.set(user.data?.email, forKey: "email")
//        UserDefaults.standard.set(user.data?.name, forKey: "name")
//        UserDefaults.standard.set(user.data?.id, forKey: "id")
//        UserDefaults.standard.set(user.data?.is_listed, forKey: "is_listed")
//        UserDefaults.standard.set(user.data?.cards_count, forKey: "cards_count")
//    }
//
//    func remove() {
//        UserDefaults.standard.removeObject(forKey: "user")
//        UserDefaults.standard.removeObject(forKey: "email")
//        UserDefaults.standard.removeObject(forKey: "name")
//        UserDefaults.standard.removeObject(forKey: "id")
//        UserDefaults.standard.removeObject(forKey: "cards_count")
//        UserDefaults.standard.removeObject(forKey: "is_listed")
//        user.token = ""
//    }
//
//    func fetchUser(){
//        if let temp = UserDefaults.standard.object(forKey: "user" ) as? String{
//            user.token = temp
//        }
//        var dataTemp = UserDetails()
//        if let temp = UserDefaults.standard.object(forKey: "email" ) as? String{
//            dataTemp.email = temp
//        }
//        if let temp = UserDefaults.standard.object(forKey: "name" ) as? String{
//            dataTemp.name = temp
//        }
//        if let temp = UserDefaults.standard.object(forKey: "id" ) as? Int{
//            dataTemp.id = temp
//        }
//
//        if let temp = UserDefaults.standard.object(forKey: "cards_count" ) as? Int{
//            dataTemp.cards_count = temp
//        }
//        if let temp = UserDefaults.standard.object(forKey: "is_listed" ) as? Bool{
//            dataTemp.is_listed = temp
//        }
//
//        print(dataTemp)
//        user.data = dataTemp
//
//
//    }
//}
//struct userData : Decodable {
//    var data : UserDetails?
//    var message : String?
//    var token : String?
//}
//struct UserDetails : Decodable {
//    var email : String?
//    var id : Int?
//    var name : String?
//    var is_listed : Bool?
//    var cards_count : Int?
//}
//struct Edit : Decodable {
//    var edit : String?
//    var editPassword : String?
//    var href : String?
//}


