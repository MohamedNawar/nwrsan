//
//  MyOrders.swift
//  nwrsan
//
//  Created by MACBOOK on 9/12/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//
import Foundation


struct  MyOrders : Decodable {
    static var Instance = MyOrders()
    private init() {}
    var data : [MyOrdersDetails]?
    
    func saveOrder(data:Data) {
        UserDefaults.standard.set(data, forKey: "MyOrders")
    }
    
    func removeOrder() {
        UserDefaults.standard.removeObject(forKey: "MyOrders")
    }
    
    mutating func fetchOrder(){
        if let  data = UserDefaults.standard.data(forKey: "MyOrders") {
            do {
                self = try JSONDecoder().decode(MyOrders.self, from: data)
                print("MyOrder")
            }catch{
                print("hey check MyOrder out!!")
            }
        }
    }
}

struct MyOrder:Decodable {
    var data : MyOrdersDetails?
}
struct MyOrdersDetails : Decodable {
    var id:Int?
    var coupon : String?
    var address:Addresses?
    var subtotal:Shipping_Price?
    var discount:Shipping_Price?
    var shipping_cost:Shipping_Price?
    var total: Shipping_Price?
    var status : Status?
    var products: [MyOrdersProducts]?
    var created_at:Created_at?
}

struct Status : Decodable {
    var id:Int?
    var qty : Int?
    var item:Int?
    var notes:String?
    var is_last:Int?
    var created_at:String?
    var updated_at: String?
    var status : StatusData?
}
struct StatusData : Decodable {
    var id:Int?
    var status : String?
    var is_default:Int?
    var created_at:String?
    var updated_at: String?
    var name : String?
    var translations:[Translations]?
}
struct Translations : Decodable {
    var id:Int?
    var order_status_id:Int?
    var name : String?
    var locale:String?
}
struct MyOrdersProducts : Decodable {
    var id:Int?
    var product : MyOrdersProductsData?
    var quantity:Int?
    var price:String?
    var total: String?
}
struct MyOrdersProductsData : Decodable {
    var id:Int?
    var name : String?
}
struct Created_at : Decodable {
    var date : String?
    var timezone_type:Int?
    var timezone:String?
}

