//
//  User.swift
//  nwrsan
//
//  Created by Mohamed Nawar on 9/8/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation


struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var identifier : String?
    
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "identifier")
        token = ""
        identifier = ""
    }
    
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
                print("gg")
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "identifier") {
            self.identifier = data
    }
}
}


struct UserDetails : Decodable {
    var id:Int?
    var name : String?
    var avatar:String?
    var email:String?
    var mobile:String?
    var type:String?
    var is_superadmin: Bool?
    var addresses : [Addresses]?
    var links: UserDetailslinks?
    var city : CiyDataInformation?
    var latitude : String?
    var longitude : String?
    var certifications : [String]?
    var issues : [String]?
    
    var address:String?
    
}
class Addresses : Decodable {
    var id : Int?
    var fullname : String?
    var city : CiyDataInformation?
    var name : String?
    var phone : String?
    var address : String?
    var is_default : Bool?
    var shipping_price : Shipping_Price?
    var total_with_shipping_price : Shipping_Price?
    var links:Links?

}
class Shipping_Price : Decodable {
    var amount : Int?
    var currency : String?
    var amount_formated : String?
}
class Links : Decodable {
    var update_address = linksContent()
}
class linksContent : Decodable {
    var href : String?
    var method : String?
}
class UserDetailslinks : Decodable {
    var update_profile : linksContent?
    var add_address : linksContent?
}
class CityData : Decodable {
    var data = CiyDataInformation()
}
class CiyDataInformation : Decodable {
    var id : Int?
    var name : String?
    var country : CountriesDataInformation?
}
class CountriesDataInformation : Decodable {
    var id : Int?
    var `default` : Int?
    var name : String?
    
}


