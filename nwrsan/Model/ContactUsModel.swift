//
//  ContactUsModel.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation

class ContactUsModel : Decodable{
    var phone: String?
    var email : String?
    var website : String?
    var facebook : String?
    var instagram : String?
    var youtube : String?
    var about : About?
    var terms_and_conditions : About?
}

class About: Decodable {
    var id : Int?
    var name : String?
    var description : String?
}
