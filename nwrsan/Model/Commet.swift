//
//  Commet.swift
//  nwrsan
//
//  Created by Moaz Ezz on 10/3/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation


class Comments : Decodable {
    var comment : String?
//    var created_at : String?
    var created_at_formated : String?
    var user : userTemp?
    var value : Int?
}

struct userTemp : Decodable {
    var avatar : String?
    var email : String?
    var id : Int?
//    var is_superadmin : String?
    var mobile : String?
    var name : String?
    var type : String?
}

