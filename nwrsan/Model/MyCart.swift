//
//  MyCart.swift
//  nwrsan
//
//  Created by MACBOOK on 9/12/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation


struct  MyCart : Decodable {
    var data : MyCartDetails?
    
}
struct MyCartDetails : Decodable {
    var identifier:String?
    var items : [MyCartDetailsItems]?
    var total_weight:Int?
    var total:Shipping_Price?
    var url:String?
    var links:MyCartDetailsLinks?
}

struct MyCartDetailsItems : Decodable {
    var id:Int?
    var qty : Int?
    var item:Item?
    var notes:String?
    var is_last:Int?
    var created_at:String?
    var updated_at: String?
    var total: Shipping_Price?
    var old_total: Shipping_Price?
    var links:ItemLinks?
}
struct MyCartDetailsLinks : Decodable {
    var add_coupon:hrefAndMethod?
    var checkout:hrefAndMethod?
}
struct Item : Decodable {
    var id:Int?
    var name:String?
    var qty : Int?
    var weight:String?
    var price:Shipping_Price?
    var image: String?
    var url: String?
    var customAttributes: [CustomAttributes]?
    var total: Shipping_Price?
    var links: ItemLinks?
}
struct CustomAttributes : Decodable {
    var attribute_id:Int?
    var attribute:String?
    var value_id : Int?
    var value:QuantumValue?
    var type:String?
}
struct ItemLinks : Decodable {
    var delete:hrefAndMethod?
    var update:hrefAndMethod?
}


