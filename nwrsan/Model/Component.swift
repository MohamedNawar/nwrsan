//
//  Component.swift
//  nwrsan
//
//  Created by MACBOOK on 9/9/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation

class HomeData : Decodable {
    var data :HomeInfoData?
}

class HomeInfoData : Decodable {
    var featured_banner : Featured_Banner?
    var products : Products?
    var brands : Brands?
    var categories : Categories?
    var slider : [String]?
}
class Featured_Banner : Decodable {
    var id : Int?
    var position : String?
    var title : String?
    var body : String?
    var url : String?
}

//Products

class Products : Decodable {
    var data :[ProductsData]?
    var see_all : hrefAndMethod?
}
class ProductsSection : Decodable {
    var data :ProductsData?
}

class ProductsData : Decodable {
    var id : Int?
    var name : String?
    var description : String?
    var images : [Images]?
    var code : String?
    var video : String?
    var keywords : String?
    var weight : String?
    var old_price : Old_Price?
    var new_price : New_Price?
    var discount_percentage: Int?
    var qty : Int?
    var views_count : Int?
    var orders_count : Int?
    var is_featured : Bool?
    var is_published : Bool?
    var is_favorite : favoriteValue?
    var rating : Rating?
    var selectable_attributes : [Attributes]?
    var additional_attributes : [Attributes]?
    var created_at : String?
    var links : LinksInfo?
    var brand : Brand?
}
class Brand : Decodable {
    var id : Int?
    var name : String?
    var logo : String?
}

class Images : Decodable {
    var small : String?
    var medium : String?
    var large : String?
    var original : String?
}
class Old_Price : Decodable {
    var amount : Int?
    var currency : String?
    var amount_formated : String?
}
class New_Price : Decodable {
    var amount : Int?
    var currency : String?
    var amount_formated : String?
}
class Attributes : Decodable {
    var id : Int?
    var name : String?
    var type : String?
    var unit : String?
    var value: String?
    var values : [Values]?

}
class Values : Decodable {
    var id : Int?
    var value : QuantumValue?
}
struct  favoriteValue: Decodable {
    var BBool : Bool?
    var IInt : Int?
    
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self.IInt = int
            return
        }
        
        if let bool = try? decoder.singleValueContainer().decode(Bool.self) {
            self.BBool = bool
            return
        }
        
        throw FavoriteError.missingValue
    }
    
    enum FavoriteError:Error {
        case missingValue
    }
}
struct  QuantumValue: Decodable {
    var IInt : Int?
    var SString : String?
    
//    case int(Int), string(String)
    
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self.IInt = int
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self.SString = string
            return
        }
        
        throw QuantumError.missingValue
    }
    
    enum QuantumError:Error {
        case missingValue
    }
}
//class Values2 : Decodable {
//    var id : Int?
//    var value : Int?
//}


class Rating : Decodable {
    var rating_average : Int?
    var rating_count : Int?
}
class LinksInfo : Decodable {
    var add_to_favorite : hrefAndMethod?
    var add_to_cart : hrefAndMethod?
    var rate:hrefAndMethod?
    var remove_from_favorite:hrefAndMethod?
}
class hrefAndMethod : Decodable {
    var href : String?
    var method : String?
}
//Brands
class Brands : Decodable {
    var data :[BrandsData]?
    var see_all : hrefAndMethod?
}
class BrandsData : Decodable {
    var id : Int?
    var name : String?
    var logo : String?
}
//categories

class Categories : Decodable {
    var data :[CategoriesData]?
    var see_all : hrefAndMethod?
}
class CategoriesSections : Decodable {
    var data :CategoriesData?
}
class CategoriesData : Decodable {
    var id : Int?
    var name : String?
    var image : String?
    var subcategories : [Subcategories]?
}
class Subcategories : Decodable {
    var id : Int?
    var name : String?
    var image : String?
}
class Favorites : Decodable {
    var data :[ProductsData]?
    var links : favoritesLinks?
    var meta : Meta?
}
class favoritesLinks : Decodable {
    var first : String?
    var last : String?
    var prev : String?
    var next : String?
}
class Meta : Decodable {
    var current_page : Int?
    var from : Int?
    var last_page : Int?
    var path : String?
    var per_page : Int?
    var to : Int?
    var total : Int?

}

////
////  Component.swift
////  nwrsan
////
////  Created by MACBOOK on 9/9/18.
////  Copyright © 2018 MohamedHassanNawar. All rights reserved.
////
//
//import Foundation
//import PKHUD
//import Alamofire
//import FCAlertView
//
//class HomeManger : Decodable {
//    static var Instance = HomeManger()
//    var homeData = HomeData()
//    
//    
//    func loadHomeData(){
//        
//        let header = APIs.Instance.getHeader()
//        Alamofire.request(APIs.Instance.homePage() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.homeData = try JSONDecoder().decode(HomeData.self, from: response.data!)
//                        print("wewewewewewe")
//                        print("successsss")
//                        
//                    }catch{
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
//                break
//            }
//        }
//    }
//    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
//        let alert = FCAlertView()
//        alert.avoidCustomImageTint = true
//        let updatedFrame = alert.bounds
//        alert.colorScheme = #colorLiteral(red: 0.3309341073, green: 0.6944722533, blue: 0.9116879106, alpha: 1)
//        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
//    }
//    
//    
//}
//
//class HomeData : Decodable {
//    var data :HomeInfoData?
//}
//
//class HomeInfoData : Decodable {
//    var featured_banner : Featured_Banner?
//    var products : Products?
//    var brands : Brands?
//    var categories : Categories?
//}
//class Featured_Banner : Decodable {
//    var id : Int?
//    var position : String?
//    var title : String?
//    var body : String?
//    var url : String?
//}
//
////Products
//
//class Products : Decodable {
//    var data :[ProductsData]?
//    var see_all : See_All?
//}
//
//class ProductsData : Decodable {
//    var id : Int?
//    var name : String?
//    var description : String?
//    var images : [Images]?
//    var code : String?
//    var video : String?
//    var keywords : String?
//    var weight : String?
//    var old_price : Old_Price?
//    var new_price : New_Price?
//    var qty : Int?
//    var views_count : Int?
//    var orders_count : Int?
//    var is_featured : Bool?
//    var is_published : Bool?
//    var is_favorite : Bool?
//    var rating : Rating?
//    var selectable_attributes : [String]?
//    var additional_attributes : [String]?
//    var created_at : String?
//    var links : Links?
//}
//class Images : Decodable {
//    var small : String?
//    var medium : String?
//    var large : String?
//    var original : String?
//}
//class Old_Price : Decodable {
//    var amount : Int?
//    var currency : String?
//    var amount_formated : String?
//}
//class New_Price : Decodable {
//    var amount : Int?
//    var currency : String?
//    var amount_formated : String?
//}
//class Rating : Decodable {
//    var rating_average : Int?
//    var rating_count : Int?
//}
//class Links : Decodable {
//    var add_to_favorite : Add_To_Favorite?
//    var add_to_cart : Add_To_Cart?
//}
//class Add_To_Favorite : Decodable {
//    var href : String?
//    var method : String?
//}
//class Add_To_Cart : Decodable {
//    var href : String?
//    var method : String?
//}
//class See_All : Decodable {
//    var href : String?
//    var method : String?
//}
////Products
//class Brands : Decodable {
//    var data :[BrandsData]?
//    var see_all : See_All?
//}
//class BrandsData : Decodable {
//    var id : Int?
//    var name : String?
//    var logo : String?
//}
////categories
//
//class Categories : Decodable {
//    var data :[CategoriesData]?
//    var see_all : See_All?
//}
//class CategoriesData : Decodable {
//    var id : Int?
//    var name : String?
//    var image : String?
//}
//
//
//
